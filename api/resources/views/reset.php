<html>
  <body>
    <h1>Hello, <?php echo $user->name ?></h1>

    <p>
      A <b>password request</b> for the UNHCR Real-Time WASH Utility Monitoring Platform was requested, presumably by you.
      In order to reset your password, please visit this secure link:
    </p>

    <p><b>
      <a href="<?php echo $url; ?>/#/reset?token=<?php echo $user->reset_token;?>"><?php echo $url; ?>/#/reset?token=<?php echo $user->reset_token; ?></a>
    </b></p>

    <p>If you cannot click the link, then simply paste it in your browser's address bar.</p>

    <p>If you did not request a password reset, you may safely ignore this message.</p>

  </body>
</html>