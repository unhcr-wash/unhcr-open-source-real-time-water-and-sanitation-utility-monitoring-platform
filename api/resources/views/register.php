<html>
  <body>
    <h1>Hello, <?php echo $user->name ?></h1>

    <p>
      An account for the UNHCR Real-Time WASH Utility Monitoring Platform was created for you.
    <p>

    <p>
      Your username is:
    </p>
    
    <p><b><?php echo $user->email; ?></b></p>
    
    <p>
      Your password is:
    </p> 
      
    <p><b><?php echo $password; ?></b>

    <p>
      Please visit <a href="<?php echo $url; ?>"><?php echo $url; ?></a> to
      sign in. If you cannot click the link, then simply paste it in your browser's address bar.
    </p>

  </body>
</html>