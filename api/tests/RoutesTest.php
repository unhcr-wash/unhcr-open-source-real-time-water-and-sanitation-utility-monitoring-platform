<?php

class RoutesTest extends TestCase
{
    public function testGet404() {
        $this->post('/sdjflsjfdldf');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 404);
        $this->assertSame($response->error, "CODE 404");
    }

    public function testGetRoot() {
        $this->get('/');
        $this->assertEquals($this->app->version(), $this->response->getContent());
    }

    public function testPostLogin() {
        $this->post('/login');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 400);
        $this->assertSame($response->status, "fail");
        $this->assertSame($response->message, "Validation errors");
        $this->assertObjectHasAttribute('email', $response->errors);
        $this->assertObjectHasAttribute('password', $response->errors);
        $this->assertSame($response->errors->email[0], "The email field is required.");
        $this->assertSame($response->errors->password[0], "The password field is required.");
    }

    public function testGetLogout() {
        $this->get('/logout');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 401);
        $this->assertSame($response->error, "CODE 401");
    }

    public function testPostForgot() {
        $this->post('/forgot');
        $response = json_decode($this->response->getContent());
        $this->assertSame($this->response->getStatusCode(), 400);
        $this->assertSame($response->status, "fail");
        $this->assertSame($response->message, "Validation errors");
        $this->assertSame($response->errors->email[0], "The email field is required.");
    }

    public function testGetReset() {
        $this->post('/reset');
        $response = json_decode($this->response->getContent());
        $this->assertSame($this->response->getStatusCode(), 400);
        $this->assertSame($response->status, "fail");
        $this->assertSame($response->message, "Validation errors");
        $this->assertObjectHasAttribute('token', $response->errors);
        $this->assertObjectHasAttribute('password', $response->errors);
        $this->assertSame($response->errors->token[0], "The token field is required.");
        $this->assertSame($response->errors->password[0], "The password field is required.");
    }

    public function testGetDeviceTypes() {
        $this->get('/devicetypes');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertObjectHasAttribute('count', $response);
        $this->assertEquals(count($response->data), 3);
        $this->assertEquals($response->count, 3);

        $devices = $this->getJsonResponseAsObject(true);
        $device = $devices['data'][0];
        $this->assertSame(array_keys($device), ['id', 'name', 'description', 'devices_count']);
        $this->assertIsInt($device['id']);
        $this->assertIsInt($device['devices_count']);
        $this->assertStringMatchesFormat('%s', $device['name']);
        $this->assertStringMatchesFormat('%a', $device['description']);
    }

    public function testGetDeviceTypesExport() {
        $this->getExportTest('devicetypes');
    }

    public function testGetDeviceTypeById() {
        $this->get('/devicetype/1');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);

        $device = $this->getJsonResponseAsObject(true);
        $this->assertSame(array_keys($device), ['id', 'name', 'description', 'created_at', 'updated_at', 'created_by',
        'updated_by', 'devices_count']);

        $this->assertIsInt($device['id']);
        $this->assertIsInt($device['devices_count']);
        $this->assertStringMatchesFormat('%s', $device['name']);
        $this->assertStringMatchesFormat('%a', $device['description']);
    }

    public function testGetCountries() {
        $this->get('/countries');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertObjectHasAttribute('count', $response);
        $this->assertEquals(count($response->data), 249);
        $this->assertEquals($response->count, 249);

        $country = $response->data[0];
        $this->assertObjectHasAttribute('id', $country);
        $this->assertGreaterThan(0, $country->id);

        $this->assertObjectHasAttribute('name', $country);
        $this->assertStringMatchesFormat('%s', $country->name);

        $this->assertObjectHasAttribute('code', $country);
        $this->assertStringMatchesFormat('%s', $country->code);

        $this->assertObjectHasAttribute('projects_count', $country);
        $this->assertGreaterThan(-1, $country->projects_count);
    }

    public function testGetCountriesExport() {
        $this->getExportTest('countries');
    }

    public function testGetCountryById() {
        $this->get('/country/2');
        $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);

        $country = $this->getJsonResponseAsObject(true);
        $this->assertSame(array_keys($country), ['id', 'name', 'code', 'created_at', 'updated_at', 'created_by', 'updated_by', 'projects_count']);
        $this->assertIsInt($country['id']);
        $this->assertStringMatchesFormat('%a', $country['name']);
        $this->assertRegExp('/[0-9A-Z]{2}/', $country['code']);
        $this->assertIsInt($country['projects_count']);
    }

    public function testGetProjects() {
        $this->get('/projects');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertObjectHasAttribute('count', $response);
        $this->assertGreaterThanOrEqual(count($response->data), 2);
        $this->assertGreaterThanOrEqual($response->count, 2);

        $projects = $this->getJsonResponseAsObject(true);
        $project = $projects['data'][0];
        $this->assertSame(array_keys($project), [
            'id', 'name', 'partner', 'beneficiaries',
            'latitude', 'longitude', 'country_id', 'created_by', 'devices_count', 'country'
        ]);

        foreach(['id', 'beneficiaries', 'country_id', 'devices_count'] as $field) {
            $this->assertIsInt($project[$field]);
        }

        $this->assertStringMatchesFormat('%s', $project['name']);
        $this->assertStringMatchesFormat('%s', $project['partner']);
        $this->assertIsNumeric($project['longitude']);
        $this->assertIsNumeric($project['latitude']);
        $this->assertIsArray($project['created_by']);
        $this->assertIsArray($project['country']);
    }

    public function testGetProjectsExport() {
        $this->getExportTest('projects');
    }

    public function testGetProjectById() {
        $this->get('/project/1');
        $this->assertSame($this->response->getStatusCode(), 200);

        $project = $this->getJsonResponseAsObject(true);
        $this->assertSame(array_keys($project), [
            'id', 'name', 'country_id', 'partner', 'beneficiaries',
            'latitude', 'longitude', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by', 'devices_count', 'country'
        ]);

        foreach(['id', 'beneficiaries', 'country_id', 'devices_count'] as $field) {
            $this->assertIsInt($project[$field]);
        }

        $this->assertStringMatchesFormat('%s', $project['name']);
        $this->assertStringMatchesFormat('%s', $project['partner']);
        $this->assertIsNumeric($project['longitude']);
        $this->assertIsNumeric($project['latitude']);
        $this->assertIsArray($project['created_by']);
        $this->assertIsArray($project['updated_by']);
        $this->assertIsArray($project['country']);
        $this->assertIsInt(strtotime($project['created_at']));
        $this->assertIsInt(strtotime($project['updated_at']));
    }

    public function testGetDevices() {
        $this->get('/devices');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertObjectHasAttribute('count', $response);
        $this->assertEquals(count($response->data), 3);
        $this->assertEquals($response->count, 3);

        $devices = $this->getJsonResponseAsObject(true);
        $device = $devices['data'][0];
        $this->assertSame(array_keys($device), ['id', 'eui', 'project_id', 'devicetype_id', 'readings_count', 'readings', 'project', 'devicetype', 'latest_reading']);

        foreach(['id', 'devicetype_id', 'readings_count', 'project_id'] as $field) {
            $this->assertIsInt($device[$field]);
        }

        $this->assertRegExp('/[0-9A-Z]{2}/', $device['eui']);
        $this->assertIsArray($device['readings']);
    }

    public function testGetDevicesExport() {
        $this->getExportTest('devices');
    }

    public function testGetDeviceById() {
        $this->get('/device/1');
        $this->assertSame($this->response->getStatusCode(), 200);

        $device = $this->getJsonResponseAsObject(true);
        $this->assertSame(array_keys($device), [
            'id', 'project_id', 'devicetype_id', 'eui', 'description',
            'latitude', 'longitude', 'created_at', 'updated_at', 'created_by',
            'updated_by', 'readings_count', 'devicetype', 'project'
        ]);

        foreach(['id', 'project_id', 'devicetype_id', 'readings_count'] as $field) {
            $this->assertIsInt($device[$field]);
        }

        $this->assertRegExp('/[0-9A-Z]{2}/', $device['eui']);
        $this->assertStringMatchesFormat('%a', $device['description']);
        $this->assertIsNumeric($device['longitude']);
        $this->assertIsNumeric($device['latitude']);
    }

    public function testPostTtn() {
        $this->post('/ttn');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 500);
        $this->assertSame($response->error, "CODE 500");
    }

    public function testGetReadings() {
        $this->get('/readings');
        $response = $this->getJsonResponseAsObject();
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('data', $response);
        $this->assertObjectHasAttribute('count', $response);
        $this->assertEquals(count($response->data), 5896);
        $this->assertEquals($response->count, 5896);

        $devices = $this->getJsonResponseAsObject(true);
        $device = $devices['data'][0];
        $this->assertSame(array_keys($device), ['id', 'value', 'datetime']);

        $this->assertIsNumeric($device['id']);
        $this->assertIsNumeric($device['value']);
        $this->assertNotNull($device['datetime']);
        $this->assertIsInt(strtotime($device['datetime']));
    }

    public function testGetReadingsExport() {
        $this->getExportTest('readings');
    }

    public function testGetReadingsById() {
        $this->get('/reading/1');
        $this->assertSame($this->response->getStatusCode(), 200);

        $reading = $this->getJsonResponseAsObject(true);
        $this->assertSame(array_keys($reading), ['id', 'device_id', 'datetime', 'value']);
        $this->assertIsNumeric($reading['id']);
        $this->assertIsNumeric($reading['device_id']);
        $this->assertIsNumeric(strtotime($reading['datetime']));
        $this->assertIsNumeric($reading['value']);
    }

    public function testGetReadingsByChartId() {
        $this->get('/readings/chart/1?startdate=2019-01-01&enddate=2019-01-10');
        $this->assertSame($this->response->getStatusCode(), 200);

        $chart_readings = $this->getJsonResponseAsObject();
        $this->assertSame(count($chart_readings), 6);
        $this->assertIsNumeric($chart_readings[0]->d);
         $this->assertIsNumeric($chart_readings[0]->v);
    }

    private function getJsonResponseAsObject($as_array = false) {
        return json_decode($this->response->getContent(), $as_array);
    }

    private function getExportTest($type) {
        $this->get("/{$type}/export");
        $this->assertSame($this->response->getStatusCode(), 200);
        $this->assertSame('attachment;filename="' . $type . '.xlsx"', $this->response->headers->get('content-disposition'));
        $this->assertSame('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', $this->response->headers->get('content-type'));
    }
}
