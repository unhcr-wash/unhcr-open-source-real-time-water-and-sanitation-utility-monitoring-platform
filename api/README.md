# UNHCR Real-Time Water and Sanitation Utility Monitoring Platform : API

## Coding style guide
[PSR-12](https://www.php-fig.org/psr/psr-12/) is to be as the coding style guide and should adhere to PSR-1 / PSR-2

## Install dependencies
Install Composer by following the instructions: https://getcomposer.org/download/

Upon succesfully installing Composer, install the dependencies:
``` bash
$ cd api
$ composer install
```

## Unit tests

``` bash
# run all unit tests
$ ./vendor/bin/phpunit

# run a single unit test within a class
$ ./vendor/bin/phpunit --filter <METHOD_NAME> <CLASS_NAME>

# watcher for any PHP files are changed
$ ./vendor/bin/phpunit-watcher watch
```
