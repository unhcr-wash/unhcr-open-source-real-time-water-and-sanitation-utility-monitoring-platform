<?php

namespace App;

use Illuminate\Support\Facades\Gate;

abstract class Rights
{
    const ViewUsers       = 'can_view_users';
    const EditUsers       = 'can_edit_users';
    const ViewProfiles    = 'can_view_profiles';
    const EditProfiles    = 'can_edit_profiles';
    const ViewLogs        = 'can_view_logs';
    const ViewCountries   = 'can_view_countries';
    const EditCountries   = 'can_edit_countries';
    const EditProjects    = 'can_edit_projects';
    const ViewDevicetypes = 'can_view_devicetypes';
    const EditDevicetypes = 'can_edit_devicetypes';
    const EditDevices     = 'can_edit_devices';

    public static function getNames()
    {
        $reflect = new \ReflectionClass(__CLASS__);
        return array_values($reflect->getConstants());
    }

    public static function check(string $name)
    {
        if (Gate::denies($name)) {
            abort(403);
        }
    }

    public static function can(string $name)
    {
        return Gate::allows($name);
    }
}
