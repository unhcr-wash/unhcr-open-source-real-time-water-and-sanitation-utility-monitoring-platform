<?php

namespace App\Http\Controllers;

use App\Log as LogItem;
use App\Project;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProjectController extends CrudController
{
    private function getList(Request $request) {
      // Find ID of logged-in user (if any, else zero).
      $userID = Auth::user() ? Auth::user()->id : 0;
      
      $query = Project::
            with('country:id,name,code') // id field must be included
          ->select(['id', 'name', 'partner', 'beneficiaries', 'latitude', 'longitude', 'public', 'country_id'])
          ->withCount('devices')
          ->where(function($query) use ($userID) {
            /* $query->whereHas('users', function (Builder $query) {
              $query->where('userID', $userID)
            });*/
            $query->where('public', 1);
            // If a user is currently logged in, show also non-public projects
            // belonging to the current user.
            if($userID) $query->orWhere('created_by', $userID);
          });

      return $this->queryFiltered($request, $query, [
          "country" => ['fields' => 'country_id'],
          "user"    => ['fields' => 'created_by'],
          "q"       => ['fields' => ['name', 'partner']], // A filter may work on multiple fields (with OR operator)
      ]);
    }

    /**
     * Retrieves a list of projects using filter
     * and order specified in request.
     */
    public function getSome(Request $request)
    {
        // No rights check here - public access.
        
        return $this->getList($request);
    }

    /**
     * Exports a list of projects to Excel using
     * filter and order specified in request.
     */
    public function export(Request $request)
    {
        // No rights check here - public access.

        $items = $this->getList($request);

        return $this->exportTo($request->input('format'), 'projects', $items['data'], [
            'name' => 'Name',
            'partner' => 'Partner',
            'country.name' => 'Country',
            'createdBy.name' => 'User',
            "devices_count" => 'Devices',
            'beneficiaries' => 'Beneficiaries',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ]);
    }

    /**
     * Retrieves the project for the given ID.
     */
    public function getOne($id)
    {
        // No rights check here - public access.

        $project = Project::
            withCount('devices')
            ->with('country:id,name,code')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->findOrFail($id);

        // Find ID of logged-in user (if any, else zero).
        $userID = Auth::user() ? Auth::user()->id : 0;

        // If project is neither public nor owned by current user, then abort.
        if(!$project->public && $project->createdBy->id != $userID) abort(403);

        return $project;
    }

    /**
     * Retrieves statistics for the project with the given ID.
     */
    public function getStats($id)
    {
      $res = DB::select(DB::raw("CALL spProjectStatistics(:id)" ), array('id' => $id));      
      return json_encode($res[0]);
    }

    /**
     * Creates a new Project with data from request.
     */
    public function create(Request $request)
    {
        // No rights check here, but routes/web.php makes sure that
        // user is logged in.

        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:projects",
            // Country must be present.
            'country' => "required",
            // Timezone must be present.
            'timezone' => "required",
            // Beneficiaries must be present and a positive integer
            'beneficiaries' => "required|integer|min:0",
            // Latitude must be present and a floating point number
            'latitude' => "required|numeric",
            // Longitude must be present and a floating point number
            'longitude' => "required|numeric",
            // Public must be present
            'public' => "required" 
        ]);

        $project = new Project();

        $project->name = $request->input('name');
        $project->partner = $request->input('partner');
        $project->beneficiaries = $request->input('beneficiaries');
        $project->description = $request->input('description');
        $project->latitude = $request->input('latitude');
        $project->longitude = $request->input('longitude');
        $project->public = $request->input('public');

        // Get country ID from country array.
        $project->country_id = $request->input('country')['id'];
        // Get timezone ID from timezone array.
        $project->timezone = $request->input('timezone')['id'];

        $project->created_by = Auth::user()->id;
        $project->updated_by = Auth::user()->id;
        $project->save();

        $this->log("Created project {$project->name}");
    }

    /**
     * Updates Project identified by ID with data from request.
     */
    public function update(Request $request, $id)
    {
        // No rights check here, but routes/web.php makes sure that
        // user is logged in.

        // Find project by ID.
        $project = Project::findOrFail($id);

        // Verify that current user created this project OR that the user
        // has authorization to edit all projects:
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditProjects)) {
            abort(403);
        }

        // Validate input:
        $name = $project->name;
        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:projects,name,{$name},name",
            // Country must be present.
            'country' => "required",
            // Timezone must be present
            'timezone' => "required",
            // Beneficiaries must be present and a positive integer
            'beneficiaries' => "required|integer|min:0",
            // Latitude must be present and a floating point number
            'latitude' => "required|numeric",
            // Longitude must be present and a floating point number
            'longitude' => "required|numeric",
            // Public must be present
            'public' => "required"             
        ]);

        $project->name = $request->input('name');
        $project->partner = $request->input('partner');
        $project->beneficiaries = $request->input('beneficiaries');
        $project->description = $request->input('description');
        $project->latitude = $request->input('latitude');
        $project->longitude = $request->input('longitude');
        $project->public = $request->input('public');

        // Get country ID from country array.
        $project->country_id = $request->input('country')['id'];
        // Get timezone ID from timezone array.
        $project->timezone = $request->input('timezone')['id'];

        $project->updated_by = Auth::user()->id;
        $project->save();

        $this->log("Updated project {$project->name}");
    }

    /**
     * Deletes Project identified by ID.
     */
    public function delete($id)
    {
        // No rights check here, but routes/web.php makes sure that
        // user is logged in.

        // Find project to delete.
        $project = Project::findOrFail($id);

        // Verify that current user created this project OR that the user
        // has authorization to edit all projects:
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditProjects)) {
            abort(403);
        }

        // Delete project.
        $project->delete();

        $this->log("Deleted project {$project->name}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
