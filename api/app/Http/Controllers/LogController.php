<?php

namespace App\Http\Controllers;

use App\Log as LogItem;
use App\Rights;
use Illuminate\Http\Request;

class LogController extends CrudController
{
    /**
     * Retrieve a list of Log entries using
     * filter and order specified in request.
     */
    public function getSome(Request $request)
    {
        Rights::check(Rights::ViewLogs);

        $query = LogItem::
            leftJoin('users', 'users.id', '=', 'logs.user_id')
            // with('user:id,name') // id field must be included
            ->select(['logs.id', 'logs.message', 'logs.created_at', 'users.email as useremail', 'users.name as username']); // foreign key must be present

        return $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "q" => ['fields' => ['users.name', 'message']],
            "type" => ['fields' => 'message'],
            "user" => ['fields' => 'user_id'],
            "date" => ['fields' => 'logs.created_at'],
        ]);
    }

    /**
     * Export a list of Log entries to Excel using
     * filter and order specified in request.
     */
    public function export(Request $request)
    {
        Rights::check(Rights::ViewLogs);

        $query = LogItem::
            leftJoin('users', 'users.id', '=', 'logs.user_id')
            // with('user:id,name') // id field must be included
            ->select(['logs.id', 'logs.message', 'logs.created_at', 'users.email as useremail', 'users.name as username']); // foreign key must be present

        $items = $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "q" => ['fields' => ['users.name', 'message']],
            "type" => ['fields' => 'message'],
            "user" => ['fields' => 'user_id'],
            "date" => ['fields' => 'date'],
        ]);

        return $this->exportTo($request->input('format'), 'log', $items['data'], [
            'username' => 'User',
            'message' => 'Message',
            'created_at' => 'Time',
        ]);
    }

    /**
     * Retrieves the Log (entry) for the given ID.
     */
    public function getOne($id)
    {
        Rights::check(Rights::ViewLogs);
        return LogItem::with('user:id,name')->findOrFail($id);
    }
}
