<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Log as LogItem;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;

class AuthController extends Controller
{
	public function signin(Request $request) {
    $this->validate($request, [
			'email'    => 'required',
			'password' => 'required'
    ]);

    $email = $request->input('email');
    $password = $request->input('password');

    // Find user by email, including their profile.
    // Unauthorized if email does not exist.
    // Note that the abort calls do not include a description, because
    // we don't want to give away whether an email exists in the database
    // or not.
    $user = User::with('profile')->where('email', $email)->first();
		if (!$user) abort(401);

    // Verify password.
    // Unauthorized if password does not match.
		if (!Hash::check($password, $user->password)) abort(401);

		// Regenerate api token upon login, invalidating the previous token.
    $user->api_token = Hash::make($user->email);

    // Set login date to current.
    $user->login_at = date("Y-m-d H:i:s");

    // Save user.
    $user->save();

		return [
      'id'        => $user->id,
      'name'      => $user->name,
      'email'     => $user->email,
      'api_token' => $user->api_token,
      'rights'    => $user->profile->getRights()
		];
  }

  public function signup(Request $request)
  {
      $this->validate($request, [
          // Name must be unique.
          'name' => "required|min:3|unique:users",
          // Email must be unique.
          'email' => "required|email|unique:users",
          // Password must not match disallowed regex:
          'password' => [
            'required',
            ['not_regex', '/^(.{0,7}|.{31,}|[^0-9]*|[^A-Za-z]*|[a-zA-Z0-9]*)$/' ]
          ]
      ]);

      // Find default profile. Abort if there isn't one.
      $profile = Profile::where('is_default', true)->first();
      if($profile == null) {
        abort(500, "No default profile available.");
      }
      
      $user = new User();
      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->password = Hash::make($request->input('password'));

      // Apply default profile:
      $user->profile_id = $profile->id;
      
      // Generate api token:
      $user->api_token = Hash::make($user->email);

      // Set login date to current.
      $user->login_at = date("Y-m-d H:i:s");

      $user->save();

      $this->log($user->id, "New user registration {$user->name}");

      // Send registration notification email to user:
      Mail::send('register', ['user' => $user, 'password' => $request->input('password'), 'url' => \config('mail.url')], function ($m) use ($user) {
          $m->to($user->email, $user->name);
          $m->subject('New User Registration');
      });

      return [
        'id'        => $user->id,
        'name'      => $user->name,
        'email'     => $user->email,
        'api_token' => $user->api_token,
        'rights'    => $profile->getRights()
      ];
  }  

	public function signout() {
    // Is a user currently logged in? If not, abort
    // the logout procedure.
    $user = Auth::user();
    if (!$user) {
      abort(401, "Not signed in.");
    }

    // Remove the api_token when logging out
		$user->api_token = NULL;
		$user->save();
  }

  public function forgot(Request $request) {
    $this->validate($request, [
			'email'    => 'required'
    ]);

    // Find user with specified email address:
    $email = $request->input('email');
    $user = User::where('email', $email)->first();
    if (!$user) abort(401, "Email address unknown.");

    // Set password reset token:
    $user->reset_token = Hash::make(time());
    $user->save();

    // Send reset email to user:
    Mail::send('reset', ['user' => $user, 'url' => \config('mail.url')], function ($m) use ($user) {
      $m->to($user->email, $user->name);
      $m->subject('Password reset');
    });
  }

  public function reset(Request $request) {
    $this->validate($request, [
			'token'    => 'required',
			'password' => 'required'
    ]);

    // Find user with specified reset token.
    $token = $request->input('token');
    $password = $request->input('password');
    $user = User::where('reset_token', $token)->first();
    if (!$user) abort(401, "Reset token not found.");

    // Set password
    $user->reset_token = NULL;
    $user->password = Hash::make($password);
    $user->save();
  }

  public function account(Request $request) {
    // Find user by API token
    $user = User::where('api_token', $request->input('api_token'))->first();
    if(!$user) abort(401, "User not found.");

    // Validate input:
    $username = $user->name;
    $useremail = $user->email;
    $this->validate($request, [
      // Name must be unique.
      'name'     => "required|min:3|unique:users,name,${username},name",
      // Email must be unique.
      'email'    => "required|email|unique:users,email,{$useremail},email",
      // Current password must pass hash check.
      'password' => function ($attribute, $value, $fail) use($user) {
        if (!Hash::check($value, $user->password)) {
            $fail('The provided password is incorrect.');
        }
      },
      // New password, if any, must follow password rules.
      'newpassword' => [['not_regex', '/^(.{0,7}|.{31,}|[^0-9]*|[^A-Za-z]*|[a-zA-Z0-9]*)$/']]
    ]);

    // Update user:
    $user->name = $request->input('name');
    $user->email = $request->input('email');
    // Password only updated if provided:
    $newpassword = $request->input('newpassword', '');
    if(strlen($newpassword) > 0) {
      $user->password = Hash::make($newpassword);
    }
    $user->save();
  }

  private function log($userid, $message)
  {
      $logitem = new LogItem();
      $logitem->user_id = $userid;
      $logitem->message = $message;
      $logitem->save();
  }  
}
