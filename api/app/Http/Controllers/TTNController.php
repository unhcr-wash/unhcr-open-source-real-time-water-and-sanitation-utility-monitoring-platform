<?php

namespace App\Http\Controllers;

use App\Device;
use App\Reading;
use App\Rights;
use App\Log as LogItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TTNController extends CrudController
{
    /**
     * This method is called by The Things Network (ttn)
     * when it needs to store a reading in the database.
     */
    public function ttn(Request $request)
    {
        // Get device EUI. It is always present in messages from TTN,
        // and it is called "hardware_serial". Abort if none present.
        if (!$request->has("hardware_serial")) {
            abort(500, "No hardware serial provided.");
        }
        $eui = $request->input("hardware_serial");

        // Does device exist? If not, abort.
        $device = Device::where('eui', $eui)->first();
        if ($device == null) {
            abort(500, "Device does not exist.");
        }

        // Update device last seen:
        $device->lastseen = gmdate('Y-m-d H:i:s');

        // If battery value is present, update device battery level:
        if($request->has("payload_fields.battery")) {
          $device->battery = $request->input("payload_fields.battery");
        }

        // If value is present, update device value:
        if($request->has("payload_fields.value")) {
          $device->value = $request->input("payload_fields.value");
        }

        // Save device:
        $device->save();

        // Create a new reading, but ONLY if a value is present.
        // Otherwise it's a status reading with no value.
        if($request->has("payload_fields.value")) {
          $reading = new Reading();
          $reading->datetime = gmdate('Y-m-d H:i:s');
          $reading->value = $request->input("payload_fields.value");
          // Quality and temperature _may_ be present, depending on the payload
          // decoder on TTN:
          if($request->has("payload_fields.quality")) $reading->quality = $request->input("payload_fields.quality");
          if($request->has("payload_fields.temp")) $reading->temperature = $request->input("payload_fields.temp");
          $device->readings()->save($reading);
        }
    }
}


