<?php

namespace App\Http\Controllers;

use App\Country;
use App\Log as LogItem;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CountryController extends CrudController
{
    /**
     * Retrieves a list of countries, using filter and order
     * specified in request.
     */
    public function getSome(Request $request)
    {
        // No rights check - public access

        $query = Country::
            select(['id', 'name', 'code'])
            ->withCount('projects');

        return $this->queryFiltered($request, $query, [
            "projects" => ['aggregate' => true, 'fields' => 'projects_count'],
            "id" => ['fields' => 'id'],
            "q" => ['fields' => ['name', 'code']],
        ]);
    }

    /**
     * Exports a list of countries to Excel, using filter and order
     * specified in request.
     */
    public function export(Request $request)
    {
        // No rights check - public access

        $query = Country::
            select(['id', 'name', 'code'])
            ->withCount('projects');

        $items = $this->queryFiltered($request, $query, [
            "projects" => ['aggregate' => true, 'fields' => 'projects_count'],
            "id" => ['fields' => 'id'],
            "q" => ['fields' => ['name', 'code']],
        ]);

        return $this->exportTo($request->input('format'), 'countries', $items['data'], [
            'name' => 'Name',
            'code' => 'Code',
        ]);
    }

    /**
     * Retrieves the Country for the given ID.
     */
    public function getOne($id)
    {
        // No rights check - public access

        return
        Country::
            withCount('projects')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->findOrFail($id);
    }

    /**
     * Creates a new Country with data specified
     * in the request.
     */
    public function create(Request $request)
    {
        Rights::check(Rights::EditCountries);

        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:countries",
            // Code must be unique.
            'code' => "required|min:2|max:2|unique:countries",
        ]);

        $item = new Country();
        foreach ($request->except('projects_count') as $field => $value) {
            $item->$field = $value;
        }
        $item->code = strtoupper($item->code); // Force country codes to be uppercase.
        $item->created_by = Auth::user()->id;
        $item->updated_by = Auth::user()->id;
        $item->save();

        $this->log("Created country {$item->name}");
    }

    /**
     * Updates Country with specified ID with data from request.
     */
    public function update(Request $request, $id)
    {
        Rights::check(Rights::EditCountries);

        // Find Country by ID.
        $item = Country::findOrFail($id);

        // Validate input:
        $name = $item->name;
        $code = strtoupper($item->code);
        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:countries,name,{$name},name",
            // Code must be unique.
            'code' => "required|min:2|max:2|unique:countries,code,{$code},code",
        ]);

        foreach ($request->except('projects_count', 'created_by', 'updated_by', 'created_at', 'updated_at') as $field => $value) {
            $item->$field = $value;
        }
        $item->code = strtoupper($item->code);
        $item->updated_by = Auth::user()->id;
        $item->save();

        $this->log("Updated country {$item->name}");
    }

    /**
     * Deletes Country with specified ID.
     */
    public function delete($id)
    {
        Rights::check(Rights::EditCountries);

        // Find item to delete.
        $item = Country::findOrFail($id);

        // Delete item.
        $item->delete();

        $this->log("Deleted country {$item->name}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
