<?php

namespace App\Http\Controllers;

use App\Devicetype;
use App\Log as LogItem;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DevicetypeController extends CrudController
{
    /**
     * Retrieves a list of DeviceTypes using filter
     * and order specified in request.
     */
    public function getSome(Request $request)
    {
        // No rights check - public access

        $query = Devicetype::
            select(['id', 'name', 'description', 'type'])
            ->withCount('devices');

        return $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "q"  => ['fields' => ['name', 'description']],
        ]);
    }

    /**
     * Exports a list of DeviceTypes to Excel using
     * filter and order specified in request.
     */
    public function export(Request $request)
    {
        // No rights check - public access

        $query = Devicetype::
            select(['id', 'name', 'description', 'type'])
            ->withCount('devices');

        $items = $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "q"  => ['fields' => ['name', 'description']],
        ]);

        return $this->exportTo($request->input('format'), 'devicetypes', $items['data'], [
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type'
        ]);
    }

    /**
     * Retrieves the Devicetype with the given ID.
     */
    public function getOne($id)
    {
        // No rights check - public access

        return
        Devicetype::
            withCount('devices')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->findOrFail($id);
    }

    /**
     * Creates a new DeviceType with data contained
     * in the request.
     */
    public function create(Request $request)
    {
        Rights::check(Rights::EditDevicetypes);

        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:devicetypes",
        ]);

        $item = new Devicetype();
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->type = $request->input('type');
        $item->created_by = Auth::user()->id;
        $item->updated_by = Auth::user()->id;
        $item->save();

        $this->log("Created device type {$item->name}");
    }

    /**
     * Updates DeviceType identified by specified ID
     * with data contained in the request.
     */
    public function update(Request $request, $id)
    {
        Rights::check(Rights::EditDevicetypes);

        // Find Devicetype by ID.
        $item = Devicetype::findOrFail($id);

        // Validate input:
        $name = $item->name;
        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|max:255|unique:devicetypes,name,{$name},name",
        ]);

        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->type = $request->input('type');
        $item->updated_by = Auth::user()->id;
        $item->save();

        $this->log("Updated device type {$item->name}");
    }

    /**
     * Deletes DeviceType identified by specified ID.
     */
    public function delete($id)
    {
        Rights::check(Rights::EditDevicetypes);

        // Find item to delete.
        $item = Devicetype::findOrFail($id);

        // Delete item.
        $item->delete();

        $this->log("Deleted device type {$item->name}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
