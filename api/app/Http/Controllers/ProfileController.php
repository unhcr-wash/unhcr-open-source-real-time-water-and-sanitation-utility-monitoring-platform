<?php

namespace App\Http\Controllers;

use App\Log as LogItem;
use App\Profile;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends CrudController
{
    /**
     * Retrieves a list of profiles using filter
     * and order specified in request.
     */
    public function getSome(Request $request)
    {
        Rights::check(Rights::ViewProfiles);

        $query = Profile::
            select(['id', 'name', 'is_default']) // must be before withCount.
            ->withCount('users');

        return $this->queryFiltered($request, $query, [
            "users" => ['aggregate' => true, 'fields' => 'users_count'],
            "id" => ['fields' => 'id'],
            "q" => ['fields' => 'name'],
        ]);
    }

    /**
     * Exports a list of profiles to Excel using
     * filter and order specified in request.
     */
    public function export(Request $request)
    {
        Rights::check(Rights::ViewProfiles);

        $query = Profile::
            select(['id', 'name', 'is_default']) // must be before withCount.
            ->withCount('users');

        $items = $this->queryFiltered($request, $query, [
            "users" => ['aggregate' => true, 'fields' => 'users_count'],
            "id" => ['fields' => 'id'],
            "q" => ['fields' => 'name'],
        ]);

        return $this->exportTo($request->input('format'), 'profiles', $items['data'], [
            'name' => 'Name',
            'users_count' => 'Users',
            'is_default' => 'Default'
        ]);
    }

    /**
     * Retrieves the profile for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function getOne($id)
    {
        Rights::check(Rights::ViewProfiles);

        return
        Profile::withCount('users')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->findOrFail($id);
    }

    /**
     * Creates a new Profile with data contained
     * in the request.
     */
    public function create(Request $request)
    {
        Rights::check(Rights::EditProfiles);

        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|unique:profiles",
        ]);

        // Create a new profile:
        $profile = new Profile();

        // Get all input fields:
        $profile->name = $request->input('name');
        $profile->is_default = $request->input('is_default');
        foreach (Rights::getNames() as $right) {
            $profile->$right = $request->input($right, false);
        }

        // Save the profile:
        $profile->created_by = Auth::user()->id;
        $profile->updated_by = Auth::user()->id;
        $profile->save();

        // If this profile is the default, then set all 
        // other profiles to non-default:
        if($profile->is_default) {
            Profile::where('id', '<>', $profile->id)->update(['is_default' => false]);
        }

        $this->log("Created profile {$profile->name}");
    }

    /**
     * Updates Profile identified by specified ID with
     * data contained in the request.
     */
    public function update(Request $request, $id)
    {
        Rights::check(Rights::EditProfiles);

        // Find profile by ID.
        $profile = Profile::findOrFail($id);

        // Validate input:
        $profilename = $profile->name;
        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|unique:profiles,name,{$profilename},name",
        ]);

        // Get all input fields
        $profile->name = $request->input('name');
        $profile->is_default = $request->input('is_default');
        foreach (Rights::getNames() as $right) {
            $profile->$right = $request->input($right, false);
        }
        $profile->updated_by = Auth::user()->id;
        $profile->save();

        // If this profile is the default, then set all 
        // other profiles to non-default:
        if($profile->is_default) {
            Profile::where('id', '<>', $profile->id)->update(['is_default' => false]);
        }        

        $this->log("Updated profile {$profile->name}");
    }

    /**
     * Deletes Profile with specified ID.
     */
    public function delete($id)
    {
        Rights::check(Rights::EditProfiles);

        // Find profile to delete.
        $profile = Profile::findOrFail($id);

        // Delete profile.
        $profile->delete();

        $this->log("Deleted profile {$profile->name}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
