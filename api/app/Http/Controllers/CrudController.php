<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CrudController extends Controller
{
    public function __construct(Request $request)
    {
        // $request->request->remove('api_token');
    }

    /**
     * Convert a url operator string into a SQL operator.
     * Unknown operator strings are always converted into '='.
     */
    private function StringToOperator(string $str)
    {
        $ops = [
            'eq' => '=',
            'neq' => '<>',
            'gt' => '>',
            'gte' => '>=',
            'lt' => '<',
            'lte' => '<=',
            'like' => 'like',
        ];
        $str = strtolower($str);
        if (array_key_exists($str, $ops)) {
            return $ops[$str];
        }

        return '=';
    }

    /**
     * Decode query URL into an array of filters.
     */
    public function getFilters(Request $request, $filterScopes)
    {
        // Only arrays are filters, and only known filters are accepted.
        $filter_inputs = [];
        foreach ($request->all() as $key => $value) {
            if (!is_array($value)) {
                continue;
            }
            // Not an array.
            if (!array_key_exists($key, $filterScopes)) {
                continue;
            }
            // Not a known filter.
            if (is_array($value)) {
                $filter_inputs[$key] = $value;
            }

        }

        $filters = [];
        foreach ($filter_inputs as $key => $op_and_values) {
            foreach ($op_and_values as $op_and_value) {
                // Does operation contain a '-' separator?
                if (strpos('-', $op_and_value) == -1) {
                    continue;
                }

                list($op, $value) = explode('-', $op_and_value, 2);
                $op = $this->StringToOperator($op);
                if ($op == 'like') {
                    $value = "%${value}%";
                }

                if (!array_key_exists($key, $filters)) {
                    $filters[$key] = [
                        "operations" => [],
                        "fields" => is_array($filterScopes[$key]["fields"]) ? $filterScopes[$key]["fields"] : [$filterScopes[$key]["fields"]],
                        "aggregate" => array_key_exists('aggregate', $filterScopes[$key]) ? $filterScopes[$key]['aggregate'] : false,
                    ];
                }
                array_push($filters[$key]["operations"], ["op" => $op, "value" => $value]);
            }
        }

        return $filters;
    }

    /*
     * Filters are provided as URL parameters, with the following format:
     *
     *   users?name[]=op-value&name[]=op-value
     *
     * Each filter has a name, an operation, and an argument.
     * Examples are:
     *
     * id[]=gte-3     id filter, with constraint >= 3
     * q[]=like-adm   q filter, with constraint LIKE %adm%
     * users[]=eq-0   users filter, with constraint = 0
     *
     * The filter name is not a database field name. This is because some
     * filters may operate on multiple fields. Instead, filter specifications
     * are provided to this method.
     *
     * The same filter may be repeated with a different operator or
     * a different value:
     *
     * id[]=lte-2&id[]=gte-5        for ids <= 2 and >= 5
     * q[]=like-adm&q[]=like-data   for q like "adm" or q like "data"
     *
     */
    public function queryFiltered(Request $request, $query, $filterScopes)
    {
        // Retrieve getSome paramaters from request.
        $offset = $request->input('offset', null);
        $count = $request->input('count', null);
        $order = $request->input('order', null);
        $dir = $request->input('dir', 'asc');

        $filters = $this->getFilters($request, $filterScopes);

        // Add WHERE filters to query.
        foreach ($filters as $filter) {
            if ($filter['aggregate']) {
                continue;
            }

            $query->where(function ($q) use ($filter) {
                foreach ($filter['operations'] as $operation) {
                    $op = $operation['op'];
                    $value = $operation['value'];
                    $q->where(function ($q) use ($filter, $op, $value) {
                        foreach ($filter['fields'] as $field) {
                            $q->orWhere($field, $op, $value);
                        }
                    });
                }
            });
        }

        // Add HAVING filters to query.
        // Note $query->having does not appear to be able to take
        // a closure, so the only way to 'or' multiple havings
        // together will be to use havingRaw.
        foreach ($filters as $filter) {
            if (!$filter['aggregate']) {
                continue;
            }

            foreach ($filter['operations'] as $operation) {
                $op = $operation['op'];
                $value = $operation['value'];
                foreach ($filter['fields'] as $field) {
                    $query->having($field, $op, $value);
                }
            }
        }

        //
        // Order:
        // This may be a single field, e.g. 'name' or multiple fields
        // separated by periods, e.g. 'employee_lastname.employee_firstname'
        // For multiple fields, order clauses are added in the order the fields
        // appear.
        //
        if ($order != null) {
            $fields = explode('.', $order);
            foreach ($fields as $field) {
                $query->orderBy($field, $dir);
            }
        }

        // Queries must be cloned, for they are mutable.
        $limitedQuery = (clone $query);
        // Add limit/offset only when provided in Request.
        if ($offset != null && $count != null) {
            $limitedQuery = $limitedQuery->offset($offset)->limit($count);
        }
        $items = $limitedQuery->get();
        $total = (clone $query)->get()->count();
        return [
            'data' => $items,
            'count' => $total,
        ];
    }

    // Returns [writer instance, filename extension, mime content type]
    private function getSheetWriter($format, $spreadsheet)
    {
        switch ($format) {
            case 'ods':return [new Ods($spreadsheet), 'ods', 'application/vnd.oasis.opendocument.spreadsheet'];
            case 'csv':return [new Csv($spreadsheet), 'csv', 'text/csv'];
            default:return [new Xlsx($spreadsheet), 'xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        }
    }

    /**
     * Reads a value from an array by path, e.g. 'profile.name'
     *
     * Note that for each record, Eloquent queries yield a collection
     * that behaves like an array, while DB queries yield a StdObj,
     * so we must cater for both.
     */
    private function readArrayByPath($arr, $path)
    {
        $keys = explode('.', $path);
        foreach ($keys as $key) {
            // Is this an array?
            if (is_array($arr)) {
                $arr = $arr[$key];
            }
            // Then it must be a StdObj.
            else {
                $arr = $arr->$key;
            }
        }
        return $arr;
    }

    /**
     * $format:    one of 'xlsx', 'csv' and 'ods'
     * $filename:  basename of file, e.g. "users"
     * $items:     list of model instances
     * $fields:    array of [fieldname => title]
     */
    public function exportTo($format, $filename, $items, $fields)
    {
        // Create a new empty spreadsheet.
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // Go through fields and display titles in first row.
        $x = 1;
        foreach ($fields as $name => $title) {
            $sheet->setCellValueByColumnAndRow($x, 1, $title);
            $x++;
        }

        // Apply bold to headers.
        $sheet->getStyleByColumnAndRow(1, 1, sizeof($fields), 1)->getFont()->setBold(true);

        // Set column dimensions to auto.
        for ($i = 1; $i <= sizeof($fields); $i++) {
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        // Go through rows and add them to sheet.
        $y = 2;
        foreach ($items as $item) {
            $x = 1;
            foreach ($fields as $name => $title) {
                $sheet->setCellValueByColumnAndRow($x, $y, $this->readArrayByPath($item, $name));
                $x++;
            }
            $y++;
        }

        // Create a writer and use it to write spreadsheet to stdout.
        list($writer, $extension, $mime) = $this->getSheetWriter($format, $spreadsheet);
        ob_start();
        $writer->save('php://output');
        $content = ob_get_clean();

        // Return binary data with correct content type.
        return response($content)
            ->withHeaders([
                'Content-type' => $mime,
                'Content-disposition' => "attachment;filename=\"${filename}.{$extension}\"",
                'Cache-control' => 'max-age=0',
                'Expires' => 'Mon, 26 Jul 1997 05:00:00 GMT',
                'Last-modified' => gmdate('D, d M Y H:i:s') . ' GMT',
                'Pragma' => 'public']);
    }
}
