<?php

namespace App\Http\Controllers;

use App\Device;
use App\Devicetype;
use App\Log as LogItem;
use App\Project;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeviceController extends CrudController
{
    private function getList(Request $request)
    {
      // Find ID of logged-in user (if any, else zero).
      $userID = Auth::user() ? Auth::user()->id : 0;      

      // Use the DB façade to get a list of devices. This cannot
      // be done using Eloquent since we need to apply CONVERT_TZ
      // the each devices's 'lastseen' value.
      $query = DB::table('devices')
      ->join('projects', 'devices.project_id', '=', 'projects.id')
      ->join('devicetypes', 'devices.devicetype_id', '=', 'devicetypes.id')
      ->select(
        'devices.id', 'devices.name', 'devices.eui', 
        'devices.value', 'devices.battery', 'devices.latitude', 'devices.longitude',  
        'projects.id AS project_id', 'projects.name AS project_name', 'projects.public as project_public',
        'devicetypes.id AS devicetype_id', 'devicetypes.name AS devicetype_name', 
        DB::raw("CONVERT_TZ(devices.lastseen, '+00:00', projects.timezone) AS lastseen"),
        DB::raw('GetMaxResult(devicetypes.type, devices.param1, devices.param2, devices.param3) AS maxResult'),
        DB::raw('GetResult(devicetypes.type, devices.value, devices.param1, devices.param2, devices.param3) AS result'),
        DB::raw('GetPeriodMax(devices.id, 1) AS max24h'),
        DB::raw('GetPeriodMean(devices.id, 1) AS avg24h'),
        DB::raw('GetPeriodMax(devices.id, 30) AS max30d'),
        DB::raw('GetPeriodMean(devices.id, 30) AS avg30d')
      )
      ->where(function($query) use ($userID) {
        $query->where('projects.public', 1);
        // If a user is currently logged in, show also non-public devices
        // belonging to a project belonging to the current user.
        if($userID) $query->orWhere('projects.created_by', $userID);
      });

      $devices = $this->queryFiltered($request, $query, [
        "id" => ['fields' => 'id'],
        "project" => ['fields' => 'project_id'],
        "devicetype" => ['fields' => 'devicetype_id'],
        "q" => ['fields' => ['name', 'eui', 'description']],
      ]);

      // Create objects for project and devicetype,
      // removing original keys. This is because the front-end
      // expects the Device record to have a shape where
      // project and devicetype are objects.
      foreach ($devices['data'] as $device) {
        $device->project = [
          "id"     => $device->project_id,
          "name"   => $device->project_name,
          "public" => $device->project_public
        ];
        $device->devicetype = [
          "id"     => $device->devicetype_id,
          "name"   => $device->devicetype_name
        ];
        unset($device->project_id);
        unset($device->project_name);
        unset($device->project_public);
        unset($device->devicetype_id);
        unset($device->devicetype_name);
      }

      // Retrieve reading counts separately, because GROUP BY blows up on MySQL server (error 1055):
      foreach($devices['data'] as $device) {
        $device->readings_count = DB::table('readings')->where('device_id', $device->id)->count();
      }

      return $devices;
    }

    /**
     * Retrieves a list of devices using filter and order
     * specified in request.
     */
    public function getSome(Request $request)
    {
        // No rights check - public access
        
        $devices = $this->getList($request);

        // For each device, get its readings for the last month (as approx. 100 points):
        $startdate = date('Y-m-d H:i:s', strtotime("-1 months"));
        $enddate = date('Y-m-d H:i:s');
        foreach ($devices['data'] as $device) {
          $device->readings = DB::select(
            DB::raw("CALL spReadings(:id, 100, :startdate, :enddate)"), array(
              "id"        => $device->id,
              "startdate" => strtotime($startdate),
              "enddate"   => strtotime($enddate)
            )
          );
        }
        return $devices;
    }

    /**
     * Exports a list of countries to Excel using filter
     * and order specified in request.
     */
    public function export(Request $request)
    {
        // No rights check - public access

        $items = $this->getList($request);

        return $this->exportTo($request->input('format'), 'devices', $items['data'], [
            'project.name' => 'Project',
            'devicetype.name' => 'Device type',
            'eui' => 'EUI',
            'name' => 'Name',
            'readings_count' => 'Readings',
            'value' => 'Last value',
            'lastseen' => 'Last seen'
        ]);
    }

    /**
     * Retrieves the Device with the given ID.
     */
    public function getOne($id)
    {
        // No rights check - public access

        $device = Device::
            with('devicetype:id,name,type')
            ->with('project:id,name,timezone,latitude,longitude')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->withCount('readings')
            ->findOrFail($id);

        // See if current user (if any) has access to view
        // this device.
        $userID = Auth::user() ? Auth::user()->id : 0;      
        $project = Project::findOrFail($device->project->id);
        $hasAccess = $project->public || $project->created_by === $userID;
        if(!$hasAccess) abort(403);

        // Get max/mean:
        $res = DB::select(DB::raw("SELECT GetPeriodMax(:id, 1) AS result"), array("id" => $device->id));
        $device->max24h = $res[0]->result;
        $res = DB::select(DB::raw("SELECT GetPeriodMean(:id, 1) AS result"), array("id" => $device->id));
        $device->avg24h = $res[0]->result;
        $res = DB::select(DB::raw("SELECT GetPeriodMax(:id, 30) AS result"), array("id" => $device->id));
        $device->max30d = $res[0]->result;
        $res = DB::select(DB::raw("SELECT GetPeriodMean(:id, 30) AS result"), array("id" => $device->id));
        $device->avg30d = $res[0]->result;

        // Convert current value to current result:
        $res = DB::select(
          DB::raw("SELECT GetResult(:type, :value, :p1, :p2, :p3) AS result" ) 
        , array(
          'type'  => $device->devicetype->type,
          'value' => $device->value,
          'p1'    => $device->param1,
          'p2'    => $device->param2,
          'p3'    => $device->param3
        ));
        $device->result = $res[0]->result;

        // Get max value:
        $res = DB::select(
          DB::raw("SELECT GetMaxValue(:type, :p1, :p2, :p3) AS max_value" ) 
        , array(
          'type'  => $device->devicetype->type,
          'p1'    => $device->param1,
          'p2'    => $device->param2,
          'p3'    => $device->param3
        ));
        $device->maxValue = $res[0]->max_value;

        // Get max result:
        $res = DB::select(
          DB::raw("SELECT GetMaxResult(:type, :p1, :p2, :p3) AS result" ) 
        , array(
          'type'  => $device->devicetype->type,
          'p1'    => $device->param1,
          'p2'    => $device->param2,
          'p3'    => $device->param3
        ));
        $device->maxResult = $res[0]->result;

        // Apply timezone conversion to 'lastseen' field.
        $res = DB::select(DB::raw("SELECT CONVERT_TZ(:lastseen, '+00:00', :timezone) AS lastseen"), array(
          'lastseen' => $device->lastseen,
          'timezone' => $device->project->timezone
        ));
        $device->lastseen = $res[0]->lastseen;

        return $device;
    }

    /**
     * Creates a new Device with the data specified
     * in the request.
     */
    public function create(Request $request)
    {
        // No rights check here, but routes/web.php makes sure that
        // user is logged in.

        $this->validate($request, [
            // project ID is required
            'project.id' => "required",
            // devicetype ID is required
            'devicetype.id' => "required",
            // Name must be under 30 characters.
            'name' => "max:30",
            // EUI must be unique.
            'eui' => "required|size:16|unique:devices",
            // Latitude must be present and a floating point number
            'latitude' => "required|numeric",
            // Longitude must be present and a floating point number
            'longitude' => "required|numeric",
        ]);

        $item = new Device();
        $item->eui = $request->input('eui');
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->latitude  = $request->input('latitude');
        $item->longitude = $request->input('longitude');
        $item->param1 = $request->input('param1');
        $item->param2 = $request->input('param2');
        $item->param3 = $request->input('param3');
        $item->created_by = Auth::user()->id;
        $item->updated_by = Auth::user()->id;

        // Find and associate to parent project:
        $project = Project::findOrFail($request->input('project')['id']);
        $item->project()->associate($project);

        // Find and associate to device type:
        $devicetype = Devicetype::findOrFail($request->input('devicetype')['id']);
        $item->devicetype()->associate($devicetype);

        // Verify that current user created the parent project OR that the user
        // has authorization to edit all projects:
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditProjects)) {
            abort(403, "User does not have permission to create a device in this project.");
        }

        $item->save();

        $this->log("Created device {$item->eui}");
    }

    /**
     * Updates Device with specified ID with data contained
     * in request.
     */
    public function update(Request $request, $id)
    {
        // No rights check here, but routes/web.php makes sure that
        // user is logged in.

        // Find Device by ID.
        $item = Device::findOrFail($id);

        // Validate input:
        $eui = $item->eui;
        $this->validate($request, [
            // devicetype is required
            'devicetype.id' => "required",
            // Name must be up to 30 characters
            "name" => "max:30",
            // EUI must be unique.
            'eui' => "required|size:16|unique:devices,eui,{$eui},eui",
            // Latitude must be present and a floating point number
            'latitude' => "required|numeric",
            // Longitude must be present and a floating point number
            'longitude' => "required|numeric",
        ]);

        $item->eui = $request->input('eui');
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->latitude  = $request->input('latitude');
        $item->longitude = $request->input('longitude');
        $item->param1 = $request->input('param1');
        $item->param2 = $request->input('param2');
        $item->param3 = $request->input('param3');        
        $item->updated_by = Auth::user()->id;

        // Find parent project:
        $project = Project::findOrFail($item->project->id);

        // Find and associate to device type:
        $devicetype = Devicetype::findOrFail($request->input('devicetype')['id']);
        $item->devicetype()->associate($devicetype);

        // Verify that current user created the parent project OR that the user
        // has authorization to edit all projects:
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditProjects)) {
            abort(403, "User does not have permission to edit a device in this project.");
        }

        $item->save();

        $this->log("Updated device {$item->eui}");
    }

    public function clear($id) 
    {
      // TODO: Rights check?

      // Find device to clear.
      $device = Device::findOrFail($id);

      // Delete all readings for device.
      $device->readings()->delete();

      $this->log("Cleared device readings for {$device->eui}");
    }

    /**
     * Deletes Device with specified ID.
     */
    public function delete($id)
    {
        // Rights::check(Rights::EditDevices); // TODO; needs proper rights check.

        // Find item to delete.
        $item = Device::findOrFail($id);

        // Delete item.
        $item->delete();

        $this->log("Deleted device {$item->eui}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
