<?php

namespace App\Http\Controllers;

use App\Device;
use App\Devicetype;
use App\Log as LogItem;
use App\Project;
use App\Rights;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends CrudController
{
    public function get(Request $request)
    {
      $res = DB::select('CALL spStatistics()');
      return json_encode($res[0]);
    }
}
