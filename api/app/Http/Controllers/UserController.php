<?php

namespace App\Http\Controllers;

use App\Log as LogItem;
use App\Rights;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends CrudController
{
    /**
     * Retrieves a list of users using filter
     * and order specified in request.
     */
    public function getSome(Request $request)
    {
        Rights::check(Rights::ViewUsers);

        $query = User::
            with('profile:id,name') // id field must be included
            ->select(['id', 'name', 'email', 'login_at', 'profile_id']); // foreign key must be present

        return $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "profile" => ['fields' => 'profile_id'],
            "q" => ['fields' => ['name', 'email']], // A filter may work on multiple fields (with OR operator).
            "date" => ['fields' => 'login_at'],
        ]);
    }

    /**
     * Exports a list of users to Excel usng
     * filter and order specified in request.
     */
    public function export(Request $request)
    {
        Rights::check(Rights::ViewUsers);

        $query = User::
            with('profile:id,name') // id field must be included
            ->select(['id', 'name', 'email', 'login_at', 'profile_id']); // foreign key must be present

        $items = $this->queryFiltered($request, $query, [
            "id" => ['fields' => 'id'],
            "profile" => ['fields' => 'profile_id'],
            "q" => ['fields' => ['name', 'email']], // A filter may work on multiple fields (with OR operator).
            "date" => ['fields' => 'login_at'],
        ]);

        return $this->exportTo($request->input('format'), 'users', $items['data'], [
            'name' => 'Name',
            'email' => 'Email',
            'login_at' => 'Last login',
            'profile.name' => 'Profile',
        ]);
    }

    /**
     * Retrieves the user with the given ID.
     */
    public function getOne($id)
    {
        Rights::check(Rights::ViewUsers);

        return
        User::with('profile:id,name')
            ->with('createdBy:id,name,email')
            ->with('updatedBy:id,name,email')
            ->findOrFail($id);
    }

    /**
     * Creates a new user with data specified in
     * the request.
     */
    public function create(Request $request)
    {
        Rights::check(Rights::EditUsers);

        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|unique:users",
            // Email must be unique.
            'email' => "required|email|unique:users",
            // Profile ID must be specified.
            'profile.id' => "required",
        ]);

        // Prepare user record:
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        // Get profile ID from profile array.
        $user->profile_id = $request->input('profile')['id'];
        // Set random password:
        $password = str_random(10);
        $user->password = Hash::make($password);
        $user->created_by = Auth::user()->id;
        $user->updated_by = Auth::user()->id;

        // Send registration notification email to user:
        Mail::send('register', ['user' => $user, 'password' => $password, 'url' => \config('mail.url')], function ($m) use ($user, $password) {
            $m->to($user->email, $user->name);
            $m->subject('New User Registration');
        });

        // Save user only if email was successfully sent:
        $user->save();

        $this->log("Created user {$user->name}");
    }

    /**
     * Updates User identified by specified ID with data
     * contained in the request.
     */
    public function update(Request $request, $id)
    {
        Rights::check(Rights::EditUsers);

        // Find user by ID.
        $user = User::findOrFail($id);

        // Validate input:
        $username = $user->name;
        $useremail = $user->email;
        $this->validate($request, [
            // Name must be unique.
            'name' => "required|min:3|unique:users,name,{$username},name",
            // Email must be unique.
            'email' => "required|email|unique:users,email,{$useremail},email",
            // Profile ID must be specified.
            'profile.id' => "required",
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        
        // Get profile ID from profile array.
        $user->profile_id = $request->input('profile')['id'];
        $user->updated_by = Auth::user()->id;
        $user->save();

        $this->log("Updated user {$user->name}");
    }

    /**
     * Delete user identified by ID.
     */
    public function delete($id)
    {
        Rights::check(Rights::EditUsers);

        // Find user to delete.
        $user = User::findOrFail($id);

        // Delete user.
        $user->delete();

        $this->log("Deleted user {$user->name}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user()->id;
        $logitem->message = $message;
        $logitem->save();
    }
}
