<?php

namespace App\Http\Controllers;

use App\Device;
use App\Project;
use App\Reading;
use App\Rights;
use App\Log as LogItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReadingController extends CrudController
{
    private function getList(Request $request) {

        // TODO: verify public access
       
        // Join the readings table to devices, then projects, in order to get 
        // the timezone for each reading (from its project). Then apply 
        // CONVERT_TZ to convert UTC+00:00 datetimes to project datetimes.
        // This requires the use of the DB façade.

        // For each reading, the user who created its project is also returned.
        // This is used to determine is a user can delete the reading.

        $query = DB::table('readings')
          ->join('devices', 'readings.device_id', '=', 'devices.id')
          ->join('projects', 'devices.project_id', '=', 'projects.id')
          ->join('devicetypes', 'devices.devicetype_id', '=', 'devicetypes.id')
          ->select(
            'readings.id', 'readings.value',  
            'readings.quality', 'readings.temperature', 
            'projects.created_by',
            DB::raw("GetResult(devicetypes.type, readings.value, devices.param1, devices.param2, devices.param3) AS result"),
            DB::raw("100 * GetResult(devicetypes.type, readings.value, devices.param1, devices.param2, devices.param3) / devices.param1 AS percentage"),
            DB::raw("CONVERT_TZ(readings.datetime, '+00:00', projects.timezone) AS datetime"));

        $readings = $this->queryFiltered($request, $query, [
          "q" => ['fields' => ['readings.value']],
          "device" => ['fields' => 'readings.device_id'],
          "date" => ['fields' => 'datetime'],
        ]);

        return $readings;
    }

    /** 
     * Retrieves list of readings, using filter and order
     * specified in request.
     */
    public function getSome(Request $request)
    {
        return $this->getList($request);
    }

    /**
     * Export list of readings to Excel, using filter
     * and order specified in request.
     */
    public function export(Request $request)
    {
        // No rights check - public access

        $items = $this->GetList($request);

        return $this->exportTo($request->input('format'), 'readings', $items['data'], [
            'datetime' => 'Datetime',
            'value' => 'Value',
            'quality' => 'Quality',
            'temperature' => 'Temperature',
            'result' => 'Result',
            'percentage' => 'Percentage'
        ]);
    }

    /**
     * Retrieves chart data for Device with specified ID,
     * with filter specified in request.
     */
    public function chart(Request $request, $id)
    {
        // No rights check - public access

        // Find start date:
        // - provided as an argument
        // - if no argument, then oldest reading for this device available in database
        // - if no reading available, then 1 year before today.
        $startdate = $request->input('startdate');
        if ($startdate == null) {
            $startdate = strtotime(DB::table('readings')->where('device_id', $id)->orderBy('datetime', 'asc')->value('datetime'));
        }
        if ($startdate == null) {
            $startdate = strtotime(date('Y-m-d', strtotime("-1 years")));
        }

        // Find end date:
        // - provided as an argument
        // - if no reading available, then today.
        $enddate = $request->input('enddate');
        if ($enddate == null) {
            $enddate = strtotime(date('Y-m-d'));
        }

        // Number of points
        $points = $request->input('points');
        if ($points == null) {
            $points = 300;
        } else {
            $points = (int) $points;
            if ($points < 0) {
                $points = 1;
            }

            if ($points > 1000) {
                $points = 1000;
            }
        }

        // Call stored procedure that returns readings.
        $readings = DB::select("CALL spReadings($id, $points, '$startdate', '$enddate')");

        return $readings;
    }

    /**
     * Retrieves the Reading for the given ID.
     */
    public function getOne($id)
    {
        // No rights check - public access

        return
        Reading::
            findOrFail($id);
    }

    /**
     * Creates a new Reading with data specified in 
     * the request.
     */
    public function create(Request $request)
    {
        // Rights::check(Rights::EditDevicetypes); // TODO

        $this->validate($request, [
            // Device ID must be present
            'device.id' => "required|integer",
            // Datetime is required and must be valid date
            'datetime' => "required|date",
            // Value is required and must be numeric
            'value' => "required|numeric",
            // Quality is required and must be an integer
            'quality' => "required|integer|min:0|max:10",
            // Temperature is required and must be an integer
            'temperature' => "required|integer|min:-20|max:50"
        ]);

        $item = new Reading();
        // Get all input fields
        foreach ($request->all() as $field => $value) {
            $item->$field = $value;
        }
        $item->save();

        $this->log("Created reading {$item->name}");
    }

    /**
     * Updates Reading identified by specified ID with
     * data from request.
     */
    public function update(Request $request, $id)
    {
        // Find reading to update. Load its device and project
        // as well, in order to determine whether the user can
        // update this reading.
        $reading = Reading::findOrFail($id);
        $device = Device::findOrFail($reading->device_id);
        $project = Project::findOrFail($device->project_id);

        // Can update only if logged in:
        if (!Auth::user()) abort(403);
        // Can update if logged in, and item was created by user
        // Can update if logged in, and user can edit devices
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditDevices)) {
          abort(403);
        }

        // Validate input:
        $this->validate($request, [
            // Value is required and must be numeric
            'value' => "required|numeric",
            // Quality is required and must be an integer
            'quality' => "required|integer|min:0|max:10",
            // Temperature is required and must be an integer
            'temperature' => "required|integer|min:-20|max:50"            
        ]);

        $reading->value = $request->input('value');
        $reading->quality = $request->input('quality');
        $reading->temperature = $request->input('temperature');
        $reading->save();

        $this->log("Updated reading {$reading->name}");
    }

    /**
     * Delete Reading with specified ID.
     */
    public function delete($id)
    {
        // Find reading to delete. Load its device and project
        // as well, in order to determine whether the user can
        // delete this reading.
        $reading = Reading::findOrFail($id);
        $device = Device::findOrFail($reading->device_id);
        $project = Project::findOrFail($device->project_id);

        // Can delete only if logged in:
        if (!Auth::user()) abort(403);
        // Can delete if logged in, and item was created by user
        // Can delete if logged in, and user can edit devices
        if ($project->created_by != Auth::user()->id && !Rights::can(Rights::EditDevices)) {
          abort(403);
        }

        // Delete reading.
        $reading->delete();

        $this->log("Deleted reading {$reading->datetime}");
    }

    private function log($message)
    {
        $logitem = new LogItem();
        $logitem->user_id = Auth::user() ? Auth::user()->id : null;
        $logitem->message = $message;
        $logitem->save();
    }
}
