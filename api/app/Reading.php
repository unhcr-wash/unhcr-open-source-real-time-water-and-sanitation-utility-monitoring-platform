<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reading extends Model
{
    public $timestamps = false;

    public function device()
    {
        return $this->belongsTo('App\Device');
    }
}
