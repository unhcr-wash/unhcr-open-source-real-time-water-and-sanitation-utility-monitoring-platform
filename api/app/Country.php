<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    // Override model table if required:
    // protected $table = 'users';
    // Override primary key if required:
    // protected $primayKey = 'id';
    // Do we have timestamps? By default yes.
    // protected $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code',
    ];

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
