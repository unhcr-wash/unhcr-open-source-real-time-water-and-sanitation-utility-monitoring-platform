<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'eui', 'description',
    ];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function devicetype()
    {
        return $this->belongsTo('App\Devicetype');
    }

    public function readings()
    {
        return $this->hasMany('App\Reading');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
