<?php

namespace App\Providers;

use App\Rights;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        //
        // Authentication is done by finding a user by their API token.
        $this->app['auth']->viaRequest('api', function ($request) {
            // If there is an API token, find the user:
            $user = null;
            if ($request->has('api_token') && $request->input('api_token') != '') {
                // Get the token.
                $token = $request->input('api_token');
                // Returns a User or NULL.
                $user = User::with('profile')->where('api_token', $token)->first();
            }
            // Define authorization gates based on user's profile.
            foreach (Rights::getNames() as $right) {
                // Define a Gate for each right in Rights:
                Gate::define($right, function ($user) use ($right) {
                    return (boolean) $user->profile->$right;
                });
            }
            return $user;
        });
    }
}
