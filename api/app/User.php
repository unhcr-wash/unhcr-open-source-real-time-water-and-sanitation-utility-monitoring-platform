<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    // Override model table if required:
    // protected $table = 'users';
    // Override primary key if required:
    // protected $primayKey = 'id';
    // Do we have timestamps? By default yes.
    // protected $timestamps = true;

    /**
     * Define default values for some of the model's
     * attributes.
     */
    protected $attributes = [
        'password' => '',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    /* public function projects() 
    {
        return $this->belongsToMany('App\Project');
    } */

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
