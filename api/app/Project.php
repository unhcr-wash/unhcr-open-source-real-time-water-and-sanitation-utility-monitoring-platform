<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country',
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

    /* public function users() 
    {
      return $this->belongsToMany('App\User');
    } */

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
