<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    /**
     * Returns an array of the rights in this profile.
     */
    public function getRights() 
    {
        // Get profile columns.
        $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());

        // Build a list of rights from profile.
        $rights = [];
        foreach($columns as $key) {
          if(substr($key, 0, 4) !== 'can_') continue;
          if($this->$key) array_push($rights, $key);
        }      

        return $rights;
    }
}
