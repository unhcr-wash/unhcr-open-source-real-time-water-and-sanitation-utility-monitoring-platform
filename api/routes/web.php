<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', function () use ($router) {
    // TODO: Anonymize this response:
    return $router->app->version();
});

$router->post('signin', 'AuthController@signin');
$router->post('signup', 'AuthController@signup');
$router->get('signout', 'AuthController@signout');
$router->post('forgot', 'AuthController@forgot');
$router->post('reset',  'AuthController@reset');

// Dashboard
$router->get('dashboard', 'DashboardController@get');

// Devicetypes
$router->get('devicetypes', 'DevicetypeController@getSome');
$router->get('devicetypes/export', 'DevicetypeController@export');
$router->get('devicetype/{id}', 'DevicetypeController@getOne');

// Countries
$router->get('countries', 'CountryController@getSome');
$router->get('countries/export', 'CountryController@export');
$router->get('country/{id}', 'CountryController@getOne');

// Projects
$router->get('projects', 'ProjectController@getSome');
$router->get('projects/export', 'ProjectController@export');
$router->get('project/stats/{id}', 'ProjectController@getStats');
$router->get('project/{id}', 'ProjectController@getOne');

// Devices
$router->get('devices', 'DeviceController@getSome');
$router->get('devices/export', 'DeviceController@export');
$router->get('device/{id}', 'DeviceController@getOne');

// Readings
$router->get('readings', 'ReadingController@getSome');
$router->get('readings/export', 'ReadingController@export');
$router->get('reading/{id}', 'ReadingController@getOne');
$router->get('readings/chart/{id}', 'ReadingController@chart');

// The Things Network
$router->post('ttn', 'TTNController@ttn');

// routes requiring authentication
$router->group(['middleware' => 'auth'], function() use ($router) {
    $router->post('account', 'AuthController@account');

    // Users
    $router->get('users', 'UserController@getSome');
    $router->get('users/export', 'UserController@export');
    $router->get('user/{id}', 'UserController@getOne');
    $router->post('user', 'UserController@create');
    $router->put('user/{id}', 'UserController@update');
    $router->delete('user/{id}', 'UserController@delete');

    // Profiles
    $router->get('profiles', 'ProfileController@getSome');
    $router->get('profiles/export', 'ProfileController@export');
    $router->get('profile/{id}', 'ProfileController@getOne');
    $router->post('profile', 'ProfileController@create');
    $router->put('profile/{id}', 'ProfileController@update');
    $router->delete('profile/{id}', 'ProfileController@delete');

    // Logs
    $router->get('logs', 'LogController@getSome');
    $router->get('logs/export', 'LogController@export');

    // Devicetypes
    $router->post('devicetype', 'DevicetypeController@create');
    $router->put('devicetype/{id}', 'DevicetypeController@update');
    $router->delete('devicetype/{id}', 'DevicetypeController@delete');

    // Countries
    $router->post('country', 'CountryController@create');
    $router->put('country/{id}', 'CountryController@update');
    $router->delete('country/{id}', 'CountryController@delete');

    // Projects
    $router->post('project', 'ProjectController@create');
    $router->put('project/{id}', 'ProjectController@update');
    $router->delete('project/{id}', 'ProjectController@delete');

    // Devices
    $router->post('device', 'DeviceController@create');
    $router->delete('device-clear/{id}', 'DeviceController@clear');
    $router->put('device/{id}', 'DeviceController@update');
    $router->delete('device/{id}', 'DeviceController@delete');

    // Readings
    $router->post('reading', 'ReadingController@create');
    $router->put('reading/{id}', 'ReadingController@update');
    $router->delete('reading/{id}', 'ReadingController@delete');
});
