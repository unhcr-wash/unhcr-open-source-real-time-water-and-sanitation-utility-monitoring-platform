import { TRight } from "./config/Config";

//
// A menu is a collection of submenus, each with a title.
// 
interface IMenu {
  [key:string]: ISubmenu
}

//
// A submenu is a collection of routes, each with a title.
// 
interface ISubmenu {
  [key: string]: IRoute
}

//
// A Route has a path, a title (for the TopBar) and
// a component to render. 
// 
interface IRoute {
  right: TRight | boolean;
  path: string;
  title: string;
  component: any;
}

// 
// Routes are named so that they can be referred to in a Menu.
// 
interface IRoutes {
  [key:string]: IRoute
}

export { IMenu, ISubmenu, IRoute, IRoutes };