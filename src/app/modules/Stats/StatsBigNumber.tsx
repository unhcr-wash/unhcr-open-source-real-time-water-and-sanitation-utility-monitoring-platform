import styled from '@independent-software/typeui/styles/Theme'

const StatsBigNumber = styled('span')`
  font-size: 35px;
  font-weight: 500;
  margin-right: 5px;
`;

export { StatsBigNumber };