import * as React from 'react';

import { Form } from '@independent-software/typeui/controls/Form';
import { Input } from '@independent-software/typeui/controls/Input';
import { Password } from '@independent-software/typeui/modules/Password';

interface ISignin {
  email: string;
  password: string;
}

interface ISigninFormProps {
  /** Initial form data. */
  data: ISignin;
  /** Called whenever form changes. */
  onChange: (data: ISignin, forceupdate: boolean) => void;
  /** Called whenever a field validates. Returns validation state for whole form. */
  onValidate: (valid: boolean) => void;
  /** Are form controls disabled? */
  disabled?: boolean;
  dirty?: boolean;
}

interface ISigninFormState {
  /** Current fom data */
  data: ISignin;
}

class SigninForm extends React.Component<ISigninFormProps, ISigninFormState> {
  constructor(props: ISigninFormProps) {
    super(props);

    this.state = {
      data: props.data
    };    
  }
  
  render() {
    let p = this.props;
    return (
      <div>
        <Form data={this.state.data} dirty={p.dirty} onChange={p.onChange} onValidate={p.onValidate}>
          <Form.Field 
            required={{message: "Email is required"}}
            contrast
            name="email" 
            value={this.state.data.email}
            control={(<Input disabled={p.disabled} type="text" placeholder="Email" fluid/>)}
            hint={(<React.Fragment>Please enter your email address.</React.Fragment>)}
            />
          <Form.Field 
            name="password" 
            value={this.state.data.password}
            contrast
            control={(<Password disabled={p.disabled} placeholder="Password" fluid/>)}
            hint={(<React.Fragment>Please enter your password.</React.Fragment>)}
            />            
        </Form>
      </div>
    )
  }
}

export { SigninForm, ISignin };