interface IAccount {
  name: string;
  email: string;
  password: string;
  newpassword: string;
} 

export { IAccount };