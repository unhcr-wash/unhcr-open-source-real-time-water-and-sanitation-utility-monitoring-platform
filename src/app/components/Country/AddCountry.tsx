import * as React from 'react';
import { Container, Content, BottomBar, Section } from '../../modules';
import { CountryForm } from './CountryForm';
import { CountryFactory, Country } from '../../resource/';
import { RouteComponentProps } from 'react-router';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Loader } from '@independent-software/typeui/controls/Loader';
import { Icon } from '@independent-software/typeui/controls/Icon';
import { ToastService } from '@independent-software/typeui/controls/Toast';

type TStep = 'ready' | 'saving' | 'saveError';

interface IState {
  country: Country;
  isValid: boolean;
  step: TStep;
  error: any;
  dirty?: boolean;
}

class AddCountry extends React.Component<IAuthProps & RouteComponentProps<any>, IState> {
  private country: Country;

  constructor(props: IAuthProps & RouteComponentProps<any>) {
    super(props);
    this.country = CountryFactory.create();
    this.state = {
      step: 'ready',
      isValid: false,
      error: null,
      country: this.country
    };
  }

  handleChange = (country: Country, forceupdate: boolean) => {
    this.country = Object.assign(this.country, country);
    if(forceupdate) { 
      this.setState({ country: this.country });  
    }
  }

  handleValidate = (valid: boolean) => {
    this.setState({
      isValid: valid
    })
  }  

  handleSubmit = () => {
    this.setState({ country: this.country, dirty: true });
    if(!this.state.isValid) return;    

    this.setState({error: null, step: 'saving'});
    this.country.$save(this.props.auth)
      .then(res => {
        ToastService.toast("Country created");
        this.props.history.push("/countries");
      })
      .catch(error => {
        this.setState({
          step: 'saveError',
          error: error
        })
      });
  }

  private handleCancelSave = () => {
    this.setState({ step: 'ready' });
  }

  render() {
    let p = this.props;
    return (
      <Container>
        {this.state.step == 'saving' && <Loader></Loader>}
        <React.Fragment>
          <Content>
            <Section padded>
              <CountryForm auth={p.auth} dirty={this.state.dirty} data={this.state.country} onChange={this.handleChange} onValidate={this.handleValidate}/>
            </Section>
          </Content>
          <BottomBar>
            <Button primary onClick={this.handleSubmit}><Icon name="save"/> Save</Button>
          </BottomBar>
        </React.Fragment>
        <Dialog.Xhr open={this.state.step == 'saveError'} error={this.state.error} onClose={this.handleCancelSave} onRetry={this.handleSubmit}/>
      </Container>
    );
  }
}

export { AddCountry };
