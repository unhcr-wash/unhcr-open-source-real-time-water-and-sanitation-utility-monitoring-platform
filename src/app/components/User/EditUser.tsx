import * as React from 'react';
import { Container, Content, BottomBar, Section } from '../../modules';
import { UserForm } from './UserForm';
import { UserFactory, User } from '../../resource/'; 
import { RouteComponentProps } from 'react-router';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Loader } from '@independent-software/typeui/controls/Loader';
import { Icon } from '@independent-software/typeui/controls/Icon';
import { ToastService } from '@independent-software/typeui/controls/Toast';
import { Message } from '@independent-software/typeui/controls/Message';

type TStep = 'loading' | 'loadError' | 'ready' | 'saving' | 'saveError' | 'error';

interface IEditUserState {
  user: User;
  isValid: boolean;
  step: TStep;
  error: any;
  dirty?: boolean;
}

interface MatchParams {
  id: string;
}

class EditUser extends React.Component<IAuthProps & RouteComponentProps<MatchParams>, IEditUserState> {
  private user: User;

  constructor(props: IAuthProps & RouteComponentProps<MatchParams>) {
    super(props);

    this.state = {
      step: 'loading',
      isValid: false,
      error: null,
      user: null
    };
    
    this.loadItem();
  }

  loadItem() {
    UserFactory.get(this.props.auth, parseInt(this.props.match.params.id)).then((user) => {
      this.user = user;
      this.setState({
        step: 'ready',
        user: this.user
      });
    })
    .catch(error => {
      this.setState({
        step: 'loadError',
        error: error
      })
    });
  }

  handleChange = (user: User, forceupdate: boolean) => {
    this.user = Object.assign(this.user, user);
    if(forceupdate) { 
      this.setState({ user: this.user });
    }       
  }

  handleValidate = (valid: boolean) => {
    this.setState({
      isValid: valid
    })
  }  

  handleSubmit = () => {
    this.setState({ user: this.user, dirty: true });
    if(!this.state.isValid) return;    

    this.setState({ step: 'saving' });
    this.user.$update(this.props.auth)
      .then(res => {
        ToastService.toast("User updated");
        this.props.history.goBack();
      })
      .catch(error => {
        this.setState({
          step: 'saveError',
          error: error
        })
      });
  }

  private handleCancelLoad = () => {
    this.setState({ step: 'error' });
  }

  private handleRetryLoad = () => {
    this.setState({ step: 'loading', error: null });
    this.loadItem();
  }
  
  private handleCancelSave = () => {
    this.setState({ step: 'ready' });
  }

  render() {
    let p = this.props;
    return (
      <Container>
        {(this.state.step == 'loading' || this.state.step == 'saving') && <Loader/>}
        {this.state.step != 'loading' && this.state.step != 'loadError' && this.state.step != 'error' && 
        <React.Fragment>
          <Content>
            <Section padded>
              <UserForm auth={p.auth} dirty={this.state.dirty} data={this.state.user} onChange={this.handleChange} onValidate={this.handleValidate}/>
            </Section>
          </Content>
          <BottomBar>
            <Button primary onClick={this.handleSubmit}><Icon name="save"/> Save</Button>
          </BottomBar>
        </React.Fragment>}
        {this.state.step == 'error' && <Content>
          <Section padded>
            <Message type="error">The requested data could not be retrieved from the server.</Message>
          </Section>
        </Content>}
        <Dialog.Xhr open={this.state.step == 'loadError'} error={this.state.error} onClose={this.handleCancelLoad} onRetry={this.handleRetryLoad}/>
        <Dialog.Xhr open={this.state.step == 'saveError'} error={this.state.error} onClose={this.handleCancelSave} onRetry={this.handleSubmit}/>
      </Container>
    );
  }
}

export { EditUser };
