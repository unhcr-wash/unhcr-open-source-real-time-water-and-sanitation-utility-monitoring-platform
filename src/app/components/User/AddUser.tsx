import * as React from 'react';
import { Container, Content, BottomBar, Section } from '../../modules';
import { UserForm } from './UserForm';
import { UserFactory, User } from '../../resource/';
import { RouteComponentProps } from 'react-router';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Loader } from '@independent-software/typeui/controls/Loader';
import { Icon } from '@independent-software/typeui/controls/Icon';
import { ToastService } from '@independent-software/typeui/controls/Toast';

type TStep = 'ready' | 'saving' | 'saveError';

interface IAddUserState {
  user: User;
  isValid: boolean;
  step: TStep;
  error: any;
  dirty?: boolean;
}

class AddUser extends React.Component<IAuthProps & RouteComponentProps<any>, IAddUserState> {
  private user: User;

  constructor(props: IAuthProps & RouteComponentProps<any>) {
    super(props);
    this.user = UserFactory.create();
    this.state = {
      step: 'ready',
      isValid: false,
      error: null,
      user: this.user
    };
  }

  handleChange = (user: User, forceupdate: boolean) => {
    this.user = Object.assign(this.user, user);
  }

  handleValidate = (valid: boolean) => {
    this.setState({
      isValid: valid
    })
  }  

  handleSubmit = () => {
    this.setState({ user: this.user, dirty: true });
    if(!this.state.isValid) return;    

    this.setState({error: null, step: 'saving'});
    this.user.$save(this.props.auth)
      .then(res => {
        ToastService.toast("User created");
        this.props.history.push("/users");
      })
      .catch(error => {
        this.setState({
          step: 'saveError',
          error: error
        })
      });
  }

  private handleCancelSave = () => {
    this.setState({ step: 'ready' });
  }

  render() {
    let p = this.props;
    return (
      <Container>
        {this.state.step == 'saving' && <Loader></Loader>}
        <React.Fragment>
          <Content>
            <Section padded>
              <UserForm auth={p.auth} dirty={this.state.dirty} data={this.state.user} onChange={this.handleChange}  onValidate={this.handleValidate}/>
            </Section>
          </Content>
          <BottomBar>
            <Button primary onClick={this.handleSubmit}><Icon name="save"/> Save</Button>
          </BottomBar>
        </React.Fragment>
        <Dialog.Xhr open={this.state.step == 'saveError'} error={this.state.error} onClose={this.handleCancelSave} onRetry={this.handleSubmit}/>
      </Container>
    );
  }
}

export { AddUser };
