import * as React from 'react';
import { Container, Content, BottomBar } from '../../modules';
import { Device } from '../../resource/';
import { RouteComponentProps } from 'react-router';
import { ListDevices } from './ListDevices';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Icon } from '@independent-software/typeui/controls/Icon';

class DeviceList extends React.Component<IAuthProps & RouteComponentProps<any>, {}> {
  constructor(props: IAuthProps & RouteComponentProps<any>) {
    super(props);
    this.handleAdd = this.handleAdd.bind(this);    
    this.handleClick = this.handleClick.bind(this);
  }

  // Go to add Device view.
  handleAdd() {
    this.props.history.push('/devices/add');
  }  

  // Go to view Device view.
  handleClick(item: Device) {
    this.props.history.push(`/devices/${item.id}`);
  }  

  render() {
    let p = this.props;
    return (
      <Container>
        <Content>
          <ListDevices auth={p.auth} name="devices" onClick={this.handleClick}/>
        </Content>
        <BottomBar>
        </BottomBar>
      </Container>
    );
  }
}

export { DeviceList };
