import * as React from 'react';
import { Container, Content, BottomBar, Section } from '../../modules';
import { ProjectForm } from './ProjectForm';
import { ProjectFactory, Project } from '../../resource/';
import { RouteComponentProps } from 'react-router';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Loader } from '@independent-software/typeui/controls/Loader';
import { Icon } from '@independent-software/typeui/controls/Icon';
import { ToastService } from '@independent-software/typeui/controls/Toast';

type TStep = 'ready' | 'saving' | 'saveError';

interface IState {
  project: Project;
  isValid: boolean;
  step: TStep;
  error: any;
  dirty?: boolean;
}

class AddProject extends React.Component<IAuthProps & RouteComponentProps<any>, IState> {
  private project: Project;

  constructor(props: IAuthProps & RouteComponentProps<any>) {
    super(props);
    this.project = ProjectFactory.create();
    this.state = {
      step: 'ready',
      isValid: false,
      error: null,
      project: this.project
    };
  }

  handleChange = (project: Project, forceupdate: boolean) => {
    this.project = Object.assign(this.project, project);
    if(forceupdate) { 
      this.setState({ project: this.project });  
    }
  }

  handleValidate = (valid: boolean) => {
    this.setState({
      isValid: valid
    })
  }  

  handleSubmit = () => {
    this.setState({ project: this.project, dirty: true });
    if(!this.state.isValid) return;    

    this.setState({error: null, step: 'saving'});
    this.project.$save(this.props.auth)
      .then(res => {
        ToastService.toast("Project created");
        this.props.history.push("/projects");
      })
      .catch(error => {
        this.setState({
          step: 'saveError',
          error: error
        })
      });
  }

  private handleCancelSave = () => {
    this.setState({ step: 'ready' });
  }

  render() {
    let p = this.props;
    return (
      <Container>
        {this.state.step == 'saving' && <Loader></Loader>}
        <React.Fragment>
          <Content>
            <Section padded>
              <ProjectForm auth={p.auth} dirty={this.state.dirty} data={this.state.project} onChange={this.handleChange} onValidate={this.handleValidate}/>
            </Section>
          </Content>
          <BottomBar>
            <Button primary onClick={this.handleSubmit}><Icon name="save"/> Save</Button>
          </BottomBar>
        </React.Fragment>
        <Dialog.Xhr open={this.state.step == 'saveError'} error={this.state.error} onClose={this.handleCancelSave} onRetry={this.handleSubmit}/>
      </Container>
    );
  }
}

export { AddProject };
