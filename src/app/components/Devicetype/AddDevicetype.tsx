import * as React from 'react';
import { Container, Content, BottomBar, Section } from '../../modules';
import { DevicetypeForm } from './DevicetypeForm';
import { DevicetypeFactory, Devicetype } from '../../resource/';
import { RouteComponentProps } from 'react-router';
import { IAuthProps } from '../../services/Auth';

import { Button } from '@independent-software/typeui/controls/Button';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Loader } from '@independent-software/typeui/controls/Loader';
import { Icon } from '@independent-software/typeui/controls/Icon';
import { ToastService } from '@independent-software/typeui/controls/Toast';

type TStep = 'ready' | 'saving' | 'saveError';

interface IState {
  devicetype: Devicetype;
  isValid: boolean;
  step: TStep;
  error: any;
  dirty?: boolean;
}

class AddDevicetype extends React.Component<IAuthProps & RouteComponentProps<any>, IState> {
  private devicetype: Devicetype;

  constructor(props: IAuthProps & RouteComponentProps<any>) {
    super(props);
    this.devicetype = DevicetypeFactory.create();
    this.state = {
      step: 'ready',
      isValid: false,
      error: null,
      devicetype: this.devicetype
    };
  }

  handleChange = (devicetype: Devicetype, forceupdate: boolean) => {
    this.devicetype = Object.assign(this.devicetype, devicetype);
    if(forceupdate) { 
      this.setState({ devicetype: this.devicetype });  
    }    
  }

  handleValidate = (valid: boolean) => {
    this.setState({
      isValid: valid
    })
  }  

  handleSubmit = () => {
    this.setState({ devicetype: this.devicetype, dirty: true });
    if(!this.state.isValid) return;    

    this.setState({error: null, step: 'saving'});
    this.devicetype.$save(this.props.auth)
      .then(res => {
        ToastService.toast("Device type created");
        this.props.history.push("/devicetypes");
      })
      .catch(error => {
        this.setState({
          step: 'saveError',
          error: error
        })
      });
  }

  private handleCancelSave = () => {
    this.setState({ step: 'ready' });
  }

  render() {
    let p = this.props;
    return (
      <Container>
        {this.state.step == 'saving' && <Loader></Loader>}
        <React.Fragment>
          <Content>
            <Section padded>
              <DevicetypeForm auth={p.auth} dirty={this.state.dirty} data={this.state.devicetype} onChange={this.handleChange} onValidate={this.handleValidate}/>
            </Section>
          </Content>
          <BottomBar>
            <Button primary onClick={this.handleSubmit}><Icon name="save"/> Save</Button>
          </BottomBar>
        </React.Fragment>
        <Dialog.Xhr open={this.state.step == 'saveError'} error={this.state.error} onClose={this.handleCancelSave} onRetry={this.handleSubmit}/>
      </Container>
    );
  }
}

export { AddDevicetype };
