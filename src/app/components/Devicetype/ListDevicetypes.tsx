import * as React from 'react';
import { Devicetype, DevicetypeFactory } from '../../resource/'; 
import { IAuthProps, List, IListState, IListProps, SearchFilter } from '../../services/';
import { IconBar, Export } from '../../modules';

import { Form } from '@independent-software/typeui/controls/Form';
import { Dialog } from '@independent-software/typeui/controls/Dialog';
import { Panel } from '@independent-software/typeui/controls/Panel';
import { DataTable } from '@independent-software/typeui/controls/DataTable';

interface IProps extends IListProps {
  /**
   * Event is fired when an item is clicked.
   */
  onClick?: (devicetype: Devicetype) => void;
}

interface IState extends IListState<Devicetype> {
}

class ListDevicetypes extends List<Devicetype, IProps, IState> {
  constructor(props: IAuthProps & IProps) {
    super(props, DevicetypeFactory, 'name', 'asc');

    // Initialize state (list initializes its own part of the state):
    this.state = {
      ...this.state
    };
  }

  handleSearch = (value:string) => {
    this.setFilter('q', 'like', value);
  }  

  render() {
    let p = this.props;

    let filter = 
    <React.Fragment>
      <Panel.Content>
        <Form.Uncontrolled hint="Type to search records">
          <SearchFilter value={this.getFilter('q', 'like')} onSearch={this.handleSearch}/>
        </Form.Uncontrolled>
      </Panel.Content>
      <Panel.Footer>
        <Export onExport={this.handleExport}/>
      </Panel.Footer>      
    </React.Fragment>

    return (
      <React.Fragment>
        <IconBar>
          <Panel.Icon icon="tools" width={300}>
            {filter}
          </Panel.Icon>
        </IconBar> 
        <DataTable error={this.state.error} loading={this.state.loading} scrollTop={this.state.scrollTop} onScroll={this.handleScroll} data={this.state.items} onFetch={this.handleFetch} onClick={p.onClick} onOrder={this.handleOrder} order={this.getOrder()} dir={this.getDir()}>
          <DataTable.Column weight={3} label="Name" order="name" dir="asc">{(item:Devicetype) => item.name}</DataTable.Column>
          <DataTable.Column weight={1} label="Type">{(item:Devicetype) => item.type}</DataTable.Column>
          <DataTable.Column weight={1} label="Devices" order="devices_count" dir="desc" align="right">{(item:Devicetype) => item.devices_count == 0 ? '-' : item.devices_count}</DataTable.Column>
        </DataTable>
        <Dialog.Xhr open={this.state.exportError != null} error={this.state.exportError} onClose={this.handleCloseDialog}/>
      </React.Fragment>
    );
  }
}

export { ListDevicetypes };
