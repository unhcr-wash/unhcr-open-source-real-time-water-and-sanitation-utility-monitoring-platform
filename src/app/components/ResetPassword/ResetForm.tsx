import * as React from 'react';
import { IReset } from './IReset';

import { Form } from '@independent-software/typeui/controls/Form';
import { Input } from '@independent-software/typeui/controls/Input';

interface IProps {
  /** Initial form data. */
  data: IReset;
  /** Called whenever form changes. */
  onChange: (data: IReset, forceupdate: boolean) => void;
  /** Called whenever a field validates. Returns validation state for whole form. */
  onValidate: (valid: boolean) => void;
  /** Form controls disabled? */
  disabled?: boolean;  
  dirty?: boolean;
}

interface IState {
  /** Current form data. */
  data: IReset;
}

class ResetForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      data: props.data
    }
  }

  render() {
    let p = this.props;
    return (
      <Form data={this.state.data} dirty={p.dirty} onChange={p.onChange} onValidate={p.onValidate}>
        <Form.Field 
          required={{message: "Password is required."}}
          minLength={{length: 8, message: "Your password must have at least 8 characters."}}
          maxLength={{length: 30, message: "Your password must have no more than 30 characters."}}
          noPattern={{pattern: '^(.{0,7}|.{31,}|[^0-9]*|[^A-Za-z]*|[a-zA-Z0-9]*)$', message: "Your password must contain letters, digits and at least one special character."}}
          contrast
          name="password" 
          value={this.state.data.password}
          control={(<Input disabled={p.disabled} type="password" fluid 
            placeholder="Password"/>)}
          hint="Please enter your new password."/>             
      </Form>
    )
  }
}

export { ResetForm };