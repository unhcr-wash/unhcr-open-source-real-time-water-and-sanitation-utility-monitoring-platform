#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Enable extensions zip (for Composer) and gd (for php-spreadsheet).
docker-php-ext-enable zip gd

# Install prestissimo extension, which enables composer 
# to do its downloads in parallel.
composer global require hirak/prestissimo

