const path = require('path'),
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin'),
      CopyWebpackPlugin = require('copy-webpack-plugin'),
      CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  mode: 'production', 
  entry: {
    app: ['./src/app/Main.tsx'],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),  // Output directory
    filename: 'js/[name].bundle.js'         // Bundle file name struccture
  },
  devtool: '',
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      }, { 
        enforce: "pre", 
        test: /\.js$/, 
        loader: "source-map-loader" 
      }
    ]
  },
  optimization: {
    usedExports: true,
    splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all'
        }
      }
    }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'src', 'app', 'index.html') }),
    new SVGSpritemapPlugin('svg/**/*.svg', {
      sprite: {
        prefix: false,
        generate: {
          title: false
        }
      },
      output: {
        filename: 'sprites.svg'
      }       
    }),
    new CopyWebpackPlugin([
      { from: 'config.json', to: '.' },
      { from: 'favicon/android-chrome-192x192.png', to: '.' },
      { from: 'favicon/android-chrome-512x512.png', to: '.' },
      { from: 'favicon/apple-touch-icon.png', to: '.' },
      { from: 'favicon/browserconfig.xml', to: '.' },
      { from: 'favicon/favicon-16x16.png', to: '.' },
      { from: 'favicon/favicon-32x32.png', to: '.' },
      { from: 'favicon/favicon.ico', to: '.' },
      { from: 'favicon/mstile-150x150.png', to: '.' },
      { from: 'favicon/safari-pinned-tab.svg', to: '.' },
      { from: 'favicon/site.webmanifest', to: '.' },      
      { from: 'node_modules/@independent-software/typeui/spritemap.svg', to: '.'}
    ])
  ]
}