function Decoder(bytes, port) {

  function GCZ_decoder(decoded_msg)  {
    var c_dist = decoded_msg.last_reading_distance_cm.toString(16);
    c_dist = ('000' + c_dist).slice(-4);
    c_dist = c_dist.substr(2, 2)+ c_dist.substr(0, 2);
    var c_temp = decoded_msg.last_reading_temperature_centigrade.toString(16);
    c_temp = ('0' + c_temp).slice(-2);
    var c_bat = bytes[0] == 48 ? bytes[10].toString(16) : '64';
    c_bat = ('0' + c_bat).slice(-2);
    decoded_msg.payload_hex = "10"+c_dist+c_temp+c_bat ;
    return decoded_msg;
  }

  // Temperature ranges -20°C to +50°C (Variable range -127 -> 127) 
  // Formula: -([256 or 0] - Byte) 
  // If the byte is greater than 0x32 then the number required for the formula 
  // is 256 otherwise the number required for the formula is 0. 
  function calcTemperature(value) {
    return value > 0x32 ? -(256 - value) : -(0 - value);
  }
  
  // Status message
  if(bytes[0] == 0x30) {
    var src = (bytes[17] & 0xf0) >> 4;
    var srssi = (bytes[17] & 0x0f);
    var temp = calcTemperature(bytes[16]);
    var value = bytes[15]+(bytes[14]*256);
    return GCZ_decoder({
      last_reading_quality_score_src: src,
      last_reading_quality_score_srssi: srssi,
      last_reading_temperature_centigrade: bytes[16],
      last_reading_distance_cm: value,
      quality: src,
      temp: temp,
      value: value,
    });
  }
  
  // Measurement message
  if(bytes[0] == 0x10) {
    var src = (bytes[19] & 0xf0) >> 4;
    var srssi = (bytes[19] & 0x0f);
    var temp = calcTemperature(bytes[18]);
    var value = bytes[17]+(bytes[16]*256);
    // Is Sonic Result Code (SRC) 0? Then no echo was detected.
    if(src == 0) {
      return GCZ_decoder({
        last_reading_quality_score_src: srssi,
        last_reading_quality_score_srssi: src,
        last_reading_temperature_centigrade: bytes[18],
        last_reading_distance_cm: value,
        quality: src,
        temp: temp,
        value: value,
      });
    }
    // Sonic Result Code is at least 1:
    else {
      return GCZ_decoder({
        last_reading_quality_score_src: src,
        last_reading_quality_score_srssi: srssi,
        last_reading_temperature_centigrade: bytes[18],
        last_reading_distance_cm: value,
        quality: src,
        temp: temp,
        value: value,
      });
    }
  }
  
}