function Decoder(bytes, port) {

  // Temperature ranges -20°C to +50°C (Variable range -127 -> 127) 
  // Formula: -([256 or 0] - Byte) 
  // If the byte is greater than 0x32 then the number required for the formula 
  // is 256 otherwise the number required for the formula is 0. 
  function calcTemperature(value) {
    var temp = value > 0x32 ? -(256 - value) : -(0 - value);
    // If temp over 50C, values will be negative. Correct this.
    if(temp < -20) temp = 50;
    return temp;
  }
  
  // Status message
  if(bytes[0] == 0x30) {
    return {
      battery: bytes[10]
    };
  }
  
  // Measurement message
  if(bytes[0] == 0x10) {
    var src = (bytes[19] & 0xf0) >> 4;
    var srssi = (bytes[19] & 0x0f);
    var temp = calcTemperature(bytes[18]);
    var value = bytes[17]+(bytes[16]*256);
    return {
      quality: src,
      temp: temp,
      value: value,
    };
  }
}