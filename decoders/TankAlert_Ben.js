function Decoder(bytes, port) {

  function GCZ_decoder(decoded_msg)  {
    var c_dist = decoded_msg.last_reading_distance_cm.toString(16);
    c_dist = ('000' + c_dist).slice(-4);
    c_dist = c_dist.substr(2, 2)+ c_dist.substr(0, 2);
    var c_temp = decoded_msg.last_reading_temperature_centigrade.toString(16);
    c_temp = ('0' + c_temp).slice(-2);
    var c_bat = bytes[0] == 48 ? bytes[10].toString(16) : '64';
    c_bat = ('0' + c_bat).slice(-2);
    decoded_msg.payload_hex = "10"+c_dist+c_temp+c_bat ;
    return decoded_msg;
  }
  
  
  if(bytes[0]==48){
    return GCZ_decoder({
      last_reading_quality_score_src: parseInt(bytes[17].toString(16).substring(0,1),16),
      last_reading_quality_score_srssi: parseInt(bytes[17].toString(16).substring(1,2),16),
      last_reading_temperature_centigrade: bytes[16],
      last_reading_distance_cm: bytes[15]+(bytes[14]*256),
      value: bytes[15]+(bytes[14]*256),
    });
  }
  
  if(bytes[0]==16){
    if(bytes[19]<=15){
      return GCZ_decoder({
        last_reading_quality_score_src: parseInt(bytes[19].toString(16).substring(0,1),16),
        last_reading_quality_score_srssi: 0,
        last_reading_temperature_centigrade: bytes[18],
        last_reading_distance_cm: bytes[17]+(bytes[16]*256),
        value: bytes[17]+(bytes[16]*256),
      });
    }
      if(bytes[19]>=16){
      return GCZ_decoder({
        last_reading_quality_score_src: parseInt(bytes[19].toString(16).substring(0,1),16),
        last_reading_quality_score_srssi: parseInt(bytes[19].toString(16).substring(1,2),16),
        last_reading_temperature_centigrade: bytes[18],
        last_reading_distance_cm: bytes[17]+(bytes[16]*256),
        value: bytes[17]+(bytes[16]*256),
      });
    }
  }
  
  }