# UNHCR Open Source Real-Time Water and Sanitation Utility Monitoring Platform using LoRaWAN

In mid-2019 UNHCR, with support from the Dutch Government / Dutch Surge Support Water, started work on open source software platform allowing humanitarian actors, NGOs, local governments, private water utilities, or refugees to monitor their own water and sanitation infrastructure in real-time using LoRaWAN technologies and leveraging the open source platform [The Things Network](https://www.thethingsnetwork.org/map).

An instance of this platform can be seen at [wash.unhcr.org](https://wash.unhcr.org/real-time-wash-data/) where water and sanitation agencies are welcome to create a free account and host their water LoRaWAN devices. Alternatively, agencies are welcome to download the software and directly install on their own server. The code is open-source so anyone is welcome to create GitLab 'issues', 'bugs', 'enhancements' or 'suggestions' [here](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/issues) or collaborate directly on improving the code base. In addition agencies are welcome to download the code and use this as a starting point for their own water and sanitation monitoring software. 

The platform was principally designed to integrate and graphically visualize the following water and sanitation real-time LoRaWAN sensors..

* Pipe Flow Sensors (water meters)
* Water Reservoir Level Sensors (ultrasonic)
* Pipe Pressure Sensors (piezometic)
* Groundwater Level Sensors (piezometic)

However the platform could easily be adapted to include..

* Free Residual Chlorine and Turbidity Sensors
* Electric Consumption Sensors (for micro-grids, generators, or solar borehole installations)
* Environmental Sensors (water quality, air, soil, weather, stream channel)

## Wiki Pages
For more information consult the following wiki pages..

* [Wiki Home](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/wikis/home)
* [List of Devices Currently Supported](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/wikis/List-of-Devices-Currently-Supported)
* [Vision / Roadmap](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/wikis/Vision/Roadmap)
* [Platform Features](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/-/wikis/Platform%20Features)
* [Installation Instructions](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/-/wikis/Installation-Instructions-for-Partners)
* [Installation Instructions for Developers](https://gitlab.com/unhcr-wash/unhcr-open-source-real-time-water-and-sanitation-utility-monitoring-platform/-/wikis/Installation-Instructions-for-Developers)

Feel free to add suggestions for improvements, download the production code, or download the source code and contribute to the project.